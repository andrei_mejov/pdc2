package test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import pdc2.core.dataset.FileDataSet;
import pdc2.core.dataset.FilterBuilder;
import pdc2.core.expression.ColumnNameExpression;
import pdc2.core.expression.ConstantExpression;
import pdc2.core.expression.ScriptExpression;
import pdc2.core.operation.SelectOperation;
import pdc2.core.source.TestInfinityGoodsSource;
import pdc2.core.type.AbstractType;

public class FilterBuilderTest 
{
	@BeforeClass
	public static void setUpBeforeClass() throws Exception 
	{
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception 
	{
	}

	@Before
	public void setUp() throws Exception 
	{
	}

	@After
	public void tearDown() throws Exception 
	{
	}
	
	@Test
	//@Ignore
	public void testEq()
	{
		TestInfinityGoodsSource source = new  TestInfinityGoodsSource();
		SelectOperation op = new SelectOperation(source.fetch());
		op.first(100);
		op.orderBy("КОД");
		op.where(
			/*КОД = 10 OR (КОД = 2 OR КОД = 3) AND NOT НАИМЕНОВАНИЕ = 'Товар100)'*/
			FilterBuilder.begin()
				.eq(new ColumnNameExpression("КОД"), new ConstantExpression(10L)).
				or().block(
					FilterBuilder.begin()
						.eq(new ColumnNameExpression("КОД"), new ConstantExpression(2L))
						.or()						
						.eq(new ColumnNameExpression("КОД"), new ScriptExpression("3"))
						.end()
				).and().block(
					FilterBuilder.begin()
						.not()
						.eq(new ColumnNameExpression("НАИМЕНОВАНИЕ"), new ConstantExpression("Товар100"))					
						.end()
				).end()
		);
		op.execute()
			.flux()
			.take(5)
			.subscribe(System.out::println);
	}
	
	@Test
	@Ignore
	public void testGtLt()
	{
		TestInfinityGoodsSource source = new  TestInfinityGoodsSource();
		SelectOperation op = new SelectOperation(source.fetch());
		op.first(20);
		op.orderBy("КОД");
		op.where(			
			FilterBuilder.begin()
				.gt(new ColumnNameExpression("КОД"), new ConstantExpression(2L))
				.and()
				.lt(new ColumnNameExpression("КОД"), new ConstantExpression(10L))
				.end()
		);
		System.out.println(op.execute());			
	}
}
