package test;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class HSQLSampleTest 
{

	@BeforeClass
	public static void setUpBeforeClass() throws Exception 
	{
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception 
	{
	}

	@Before
	public void setUp() throws Exception 
	{
	}

	@After
	public void tearDown() throws Exception 
	{
	}

	@Test
	public void test() throws Throwable
	{
		Connection c = DriverManager.getConnection(
			"jdbc:hsqldb:file:./misc/demo/data/hsql/test;shutdown=true&ifexists=true", 
			"sa", 
			""
		);
		try {
			c.setAutoCommit(false);
			Statement s = c.createStatement();
			s.execute("CREATE TABLE TEST_TABLE (CODE INT, TITLE VARCHAR(100))");
			s.execute("INSERT INTO TEST_TABLE (CODE, TITLE) VALUES (1, 'ЙЦУКЕН')");
			s.execute("INSERT INTO TEST_TABLE (CODE, TITLE) VALUES (2, 'ФЫВАПР')");
			c.commit();
		}
		finally {
			c.close();
		}
	}
}
