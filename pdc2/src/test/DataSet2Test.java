package test;

import static org.junit.Assert.*;

import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import javax.sql.rowset.serial.SerialArray;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import pdc2.core.dataset.AbstractDataSet;
import pdc2.core.dataset.ArrayDataSet;
import pdc2.core.dataset.FileDataSet;
import pdc2.core.dataset.Index;
import pdc2.core.dataset.Row;
import pdc2.core.dataset.Value;
import pdc2.core.expression.ColumnNameExpression;
import pdc2.core.expression.ScriptExpression;
import pdc2.core.operation.JoinOperation;
import pdc2.core.operation.SelectOperation;
import pdc2.core.operation.ShowOperation;
import pdc2.core.source.JDBCSource;
import pdc2.core.source.TestInfinityGoodsSource;
import pdc2.core.type.AbstractType;
import pdc2.core.type.LongType;
import pdc2.core.type.StringType;
import reactor.core.publisher.Flux;
import reactor.core.publisher.GroupedFlux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

public class DataSet2Test 
{	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception 
	{
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception 
	{
	}

	@Before
	public void setUp() throws Exception 
	{
	}

	@After
	public void tearDown() throws Exception 
	{
	}

	@Test
	@Ignore
	public void test() 
	{
		ArrayDataSet ds = new ArrayDataSet();
		ds.column("Col1", new StringType());
		ds.column("Col2", new StringType());
		ds.column("Col3", new StringType());
		ds.row("Vl11", "Vl12", "Vl13");
		ds.row("Vl21", "Vl22", "Vl23");		
		System.out.println(ds);
		ds.flux()
			.filter( row -> {
				return row.getValues().get(0).getValue().equals("Vl11");
			}).map(row -> {
				return row;
			}).toStream().forEach(System.out::println);
		
		FileDataSet fds = new FileDataSet("../pdc/misc/test.dat");
		Flux<Row> f = fds
			.flux()
			.filter( row -> {return row.getByName("GOOD_GROUP").getValue().equals("7A120201Z");})			
			.sort( (a, b) -> {return a.getByName("CODE").compareTo(b.getByName("CODE"));});		
		ds = new ArrayDataSet(f);
		System.out.println(ds);		
	}	
							
	@Test
	//@Ignore
	public void testSource()
	{
		HashMap<String, String> parameters = new HashMap<>();
		TestInfinityGoodsSource source = new TestInfinityGoodsSource();
		source.init(parameters);
		SelectOperation op = new SelectOperation(source.fetch());
		op
			.first(10)
			.execute()
			.flux()
			.subscribe(System.out::println);
	}
}


