package test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import pdc2.core.dataset.Row;
import pdc2.core.expression.AndExpression;
import pdc2.core.expression.ConstantExpression;
import pdc2.core.expression.EqExpression;
import pdc2.core.expression.GtExpression;
import pdc2.core.expression.LtExpression;
import pdc2.core.expression.NotExpression;
import pdc2.core.expression.OrExpression;
import pdc2.core.expression.ScriptExpression;
import pdc2.core.type.AbstractType;

public class BooleanExpressionTest 
{

	@BeforeClass
	public static void setUpBeforeClass() throws Exception 
	{
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception 
	{
	}

	@Before
	public void setUp() throws Exception 
	{
	}

	@After
	public void tearDown() throws Exception 
	{
	}

	@Test
	public void testEq()
	{
		EqExpression eq = new EqExpression(
			new ConstantExpression(AbstractType.wrapValue("", 123)), 
			new ScriptExpression("100 + 23")
		);
		assertTrue((Boolean)eq.eval(new Row()).getValue());
		
		eq = new EqExpression(
				new ConstantExpression(AbstractType.wrapValue("", 123)), 
				new ScriptExpression("100 + 22")
			);
		assertFalse((Boolean)eq.eval(new Row()).getValue());
	}
	
	@Test
	public void testGt()
	{
		GtExpression gt = new GtExpression(
			new ConstantExpression(AbstractType.wrapValue("", 123)), 
			new ScriptExpression("100 + 22")
		);
		assertTrue((Boolean)gt.eval(new Row()).getValue());
		
		gt = new GtExpression(
				new ConstantExpression(AbstractType.wrapValue("", 123)), 
				new ScriptExpression("100 + 24")
			);
		assertFalse((Boolean)gt.eval(new Row()).getValue());
	}
	
	@Test
	public void testLt()
	{
		LtExpression gt = new LtExpression(
			new ConstantExpression(AbstractType.wrapValue("", 123)), 
			new ScriptExpression("100 + 24")
		);
		assertTrue((Boolean)gt.eval(new Row()).getValue());
		
		gt = new LtExpression(
				new ConstantExpression(AbstractType.wrapValue("", 123)), 
				new ScriptExpression("100 + 22")
			);
		assertFalse((Boolean)gt.eval(new Row()).getValue());
	}
	
	@Test
	public void testAnd()
	{
		EqExpression eq1 = new EqExpression(
			new ConstantExpression(AbstractType.wrapValue("", 456)), 
			new ScriptExpression("400 + 56")
		);
		EqExpression eq2 = new EqExpression(
			new ConstantExpression(AbstractType.wrapValue("", 789)), 
			new ConstantExpression(AbstractType.wrapValue("", 789))
		);
		AndExpression and = new AndExpression(eq1, eq2);
		assertTrue((Boolean)and.eval(new Row()).getValue());
		
		eq1 = new EqExpression(
			new ConstantExpression(AbstractType.wrapValue("", 456)), 
			new ScriptExpression("400 + 57")
		);
		eq2 = new EqExpression(
			new ConstantExpression(AbstractType.wrapValue("", 789)), 
			new ConstantExpression(AbstractType.wrapValue("", 789))
		);
		and = new AndExpression(eq1, eq2);
		assertFalse((Boolean)and.eval(new Row()).getValue());
	}
	
	@Test
	public void testOr()
	{
		EqExpression eq1 = new EqExpression(
			new ConstantExpression(AbstractType.wrapValue("", 456)), 
			new ScriptExpression("400 + 56")
		);
		EqExpression eq2 = new EqExpression(
			new ConstantExpression(AbstractType.wrapValue("", 789)), 
			new ConstantExpression(AbstractType.wrapValue("", 789))
		);
		OrExpression or = new OrExpression(eq1, eq2);
		assertTrue((Boolean)or.eval(new Row()).getValue());
		
		eq1 = new EqExpression(
			new ConstantExpression(AbstractType.wrapValue("", 456)), 
			new ScriptExpression("400 + 57")
		);
		eq2 = new EqExpression(
			new ConstantExpression(AbstractType.wrapValue("", 789)), 
			new ConstantExpression(AbstractType.wrapValue("", 789))
		);
		or = new OrExpression(eq1, eq2);
		assertTrue((Boolean)or.eval(new Row()).getValue());
		
		eq1 = new EqExpression(
			new ConstantExpression(AbstractType.wrapValue("", 456)), 
			new ScriptExpression("400 + 57")
		);
		eq2 = new EqExpression(
			new ConstantExpression(AbstractType.wrapValue("", 789)), 
			new ConstantExpression(AbstractType.wrapValue("", 780))
		);
		or = new OrExpression(eq1, eq2);
		assertFalse((Boolean)or.eval(new Row()).getValue());				
	}
	
	@Test
	public void testNot()
	{
		EqExpression eq = new EqExpression(
			new ConstantExpression(AbstractType.wrapValue("", 123)), 
			new ScriptExpression("100 + 23")
		);
		assertTrue((Boolean)eq.eval(new Row()).getValue());
		NotExpression not = new NotExpression(eq);
		assertFalse((Boolean)not.eval(new Row()).getValue());
	}
}
