package test;

import static org.junit.Assert.*;

import java.util.HashMap;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.lambdaworks.redis.RedisClient;
import com.lambdaworks.redis.RedisConnection;
import com.lambdaworks.redis.RedisURI;
import com.lambdaworks.redis.api.StatefulRedisConnection;

public class FillRedisTest 
{

	@BeforeClass
	public static void setUpBeforeClass() throws Exception 
	{
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception 
	{
	}

	@Before
	public void setUp() throws Exception 
	{
	}

	@After
	public void tearDown() throws Exception 
	{
	}

	@Test
	public void test() 
	{
		HashMap<String, String> rowHash = new HashMap<String, String>();
		rowHash.put("Кол1", "Знач1");
		rowHash.put("Кол2", "Знач2");
		rowHash.put("Кол3", "Знач3");		
		RedisClient redisClient = RedisClient.create(
			RedisURI.create("redis://localhost:6379")
		);
		try
		{
			StatefulRedisConnection<String, String> connection = redisClient.connect();
			try
			{				
				for (String s : connection.sync().keys("testTable:*"))
				{
					System.out.println("=>" + s);
				}
				connection.sync().hmset("testTable:" + rowHash.get("Кол1"), rowHash);				
				assertFalse(connection.sync().hgetall("testTable").isEmpty());
			}
			finally
			{
				connection.close();
			}
		}
		finally
		{
			redisClient.shutdown();
		}
	}
}
