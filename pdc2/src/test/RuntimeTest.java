package test;

import static org.junit.Assert.*;

import java.io.File;
import java.util.HashMap;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import pdc2.core.PDC2Runtime;
import pdc2.core.expression.ColumnNameExpression;
import pdc2.core.operation.JoinOperation;
import pdc2.core.source.TestInfinityBarcodesSource;
import pdc2.core.source.TestInfinityGoodsSource;

public class RuntimeTest 
{
	@BeforeClass
	public static void setUpBeforeClass() throws Exception 
	{
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception 
	{
	}

	@Before
	public void setUp() throws Exception 
	{
	}

	@After
	public void tearDown() throws Exception 
	{
	}

	@Test
	//@Ignore
	public void testSelect() 
	{
		PDC2Runtime pdc2 = new PDC2Runtime();
		pdc2.selectFromFile("./misc/demoLocal/data/sin.dat", "sin").execute();
		pdc2
			.select("sin", "sin")
			.orderBy("X")
			.first(10)
			.execute();
		pdc2.show("sin");
	}
	
	@Test
	@Ignore
	public void testJoin()
	{
		PDC2Runtime pdc2 = new PDC2Runtime();
		HashMap<String, String> parameters = new HashMap<>();
		parameters.put("class", TestInfinityGoodsSource.class.getName());
		pdc2.defineSource("goods", parameters);
		parameters.put("class", TestInfinityBarcodesSource.class.getName());
		pdc2.defineSource("barcodes", parameters);
		pdc2.select("goods", "ном").first(10).execute();
		pdc2.select("barcodes", "шк").first(10).execute();
		pdc2.join("рез")
			.left(pdc2.getDataSets().get("ном"))
			.right(pdc2.getDataSets().get("шк"))
			.leftExpression("КОД")
			.rightExpression("КОД")
			.innerJoin()
			.execute();
		pdc2.select("рез", "рез").first(10).execute();
		pdc2.show("рез");
	}
	
	@Test
	//@Ignore
	public void testGroup()
	{
		PDC2Runtime pdc2 = new PDC2Runtime();
		pdc2
			.selectFromFile("./misc/demoLocal/data/group.dat", "т1")
			.execute();
		pdc2
			.groupBy("т2")
			.from(pdc2.getDataSets().get("т1"))
			.columns("CODE")
			.max("TITLE")
			.min("COUNT")
			.sum("PRICE")
			.execute();
		pdc2.show("т2");
	}
	
	@Test
	//@Ignore
	public void testSource()
	{
		HashMap<String, String> parameters = new HashMap<>();
		parameters.put("class", "pdc2.core.source.XLSXSource");
		parameters.put("file", "./misc/demoLocal/data/test.xlsx");
		PDC2Runtime pdc2 = new PDC2Runtime();		
		pdc2.defineSource("xlsx", parameters);
		pdc2.select("xlsx", "рез").execute();
		pdc2.show("рез");
	}
	
	@Test
	//@Ignore
	public void testConsumer()
	{
		HashMap<String, String> parameters = new HashMap<>();
		parameters.put("class", "pdc2.core.consumer.XLSXConsumer");
		parameters.put("file", "/tmp/out.xlsx");
		PDC2Runtime pdc2 = new PDC2Runtime();
		pdc2.defineConsumer("xlsx", parameters);
		pdc2.selectFromFile("./misc/demoLocal/data/sin.dat", "sin").execute();
		pdc2
			.select("sin", "sin")
			.orderBy("X")
			.first(10)
			.execute();
		pdc2.show("sin");
		pdc2.saveWithConsumer("sin", "xlsx");
	}
	
	/*@Test
	@Ignore
	public void testEval()
	{
		PDC2Runtime pdc2 = new PDC2Runtime();		
		pdc2.eval(new File("./misc/test/test4.pdc2"), new HashMap<String, String>());
	}
	
	@Test
	@Ignore
	public void testEval1()
	{
		PDC2Runtime pdc2 = new PDC2Runtime();		
		pdc2.eval(new File("./misc/test/test5.pdc2"), new HashMap<String, String>());
		assertNotNull(pdc2.getReturnedDataSet());
	}
	
	@Test
	@Ignore
	public void testEval2()
	{
		PDC2Runtime pdc2 = new PDC2Runtime();		
		pdc2.eval(new File("./misc/test/test6.pdc2"), new HashMap<String, String>());		
	}
	
	@Test	
	@Ignore
	public void testEval3()
	{
		PDC2Runtime pdc2 = new PDC2Runtime();		
		pdc2.eval(new File("./misc/test/test8.pdc2"), new HashMap<String, String>());		
	}
	
	@Test	
	@Ignore
	public void testEval4()
	{
		PDC2Runtime pdc2 = new PDC2Runtime();		
		pdc2.eval(new File("./misc/test/test9.pdc2"), new HashMap<String, String>());		
	}
	
	@Test	
	@Ignore
	public void testEval5()
	{
		PDC2Runtime pdc2 = new PDC2Runtime();		
		pdc2.eval(new File("./misc/test/test10.pdc2"), new HashMap<String, String>());		
	} */
}
