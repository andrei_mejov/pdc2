package test;

import static org.junit.Assert.*;

import java.text.SimpleDateFormat;
import java.util.Locale;

import org.junit.Test;

public class ParseDateTest 
{

	@Test
	public void test() throws Throwable
	{
		String dateString = "4-Oct-06";
		SimpleDateFormat sdf = new SimpleDateFormat("d-MMM-yy", Locale.US);
		System.out.println(sdf.parse(dateString));
	}

}
