package test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import pdc2.core.dataset.FileDataSet;
import pdc2.core.dataset.FluxDataSet;
import pdc2.core.dataset.Row;
import pdc2.core.dataset.Value;
import pdc2.core.expression.ScriptExpression;
import pdc2.core.operation.SelectOperation;
import pdc2.core.source.TestInfinityGoodsSource;
import pdc2.core.type.AbstractType;
import pdc2.core.type.LongType;
import pdc2.core.type.StringType;
import reactor.core.publisher.Flux;

public class SelectOperationTest 
{	
	private Flux<Row> resFlux = Flux.empty();

	@BeforeClass
	public static void setUpBeforeClass() throws Exception 
	{
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception 
	{
	}

	@Before
	public void setUp() throws Exception 
	{
	}

	@After
	public void tearDown() throws Exception 
	{
	}

	
	@Test
	@Ignore
	public void testSelect()
	{
		TestInfinityGoodsSource goods = new TestInfinityGoodsSource();
		FluxDataSet fds = new FluxDataSet(goods.fetch().flux().take(100L));
		SelectOperation s = new SelectOperation(fds);
		s.where(row -> {
				return row.getByName("КОД").getValue().equals(11L) ||
					row.getByName("КОД").getValue().equals(15L);
			})		
			.project("КОД", "НАИМЕНОВАНИЕ")
			.cast("КОД", new StringType())
			.orderByDesc("КОД");		
		
		SelectOperation s1 = new SelectOperation(fds);
		s1.where(row -> {				
				return row.getByName("КОД").getValue().equals(16L);
			})		
			.project("КОД", "НАИМЕНОВАНИЕ")
			.cast("КОД", new StringType())
			.orderByDesc("КОД");
		
		s.union(s1.getOperationFlux())
			.first(5);
		System.out.println("---------------------");
		System.out.println(s.execute());
		System.out.println("---------------------");
	}
	
	@Test
	@Ignore
	public void testGroupBy()
	{
		TestInfinityGoodsSource goods = new TestInfinityGoodsSource();
		FluxDataSet fds = new FluxDataSet(goods.fetch().flux().take(100L));
		fds
			.flux()
			.map( row -> {
				row.getValues().add(
					AbstractType.wrapValue("ГРУППА", 1L)
				);
				return row;
			}).groupBy( row -> {return row.getByName("ГРУППА").getValue();})			
			.subscribe( gf -> {
				Value groupCode = AbstractType.wrapValue("ГРУППА", gf.key());
				Value count = new Value();
				count.setName("COUNT");
				count.setType(new LongType());
				count.setValue(0L);
				Row row = new Row();
				row.getValues().add(groupCode);
				row.getValues().add(count);			
				gf.reduce(row, (a, b) -> {
					long c = (long)a.getByName("COUNT").getValue();
					c++;
					a.getByName("COUNT").setValue(c);					
					return a;
				}).subscribe( r -> {resFlux = resFlux.concatWithValues(r);});				
			});
		resFlux
			.sort( (a, b) -> {return a.getByName("COUNT").compareTo(b.getByName("COUNT"));})
			.subscribe(System.out::println);		
	}
	
	@Test
	@Ignore
	public void testRename()
	{
		TestInfinityGoodsSource goods = new TestInfinityGoodsSource();
		FluxDataSet fds = new FluxDataSet(goods.fetch().flux().take(100L));
		SelectOperation op = new SelectOperation(fds);
		op
			.rename("КОД", "CODE")
			.execute()
				.flux()
					.take(5)
					.subscribe(System.out::println);
	}
	
	@Test
	@Ignore
	public void testScriptExpression()
	{
		ScriptExpression se = new ScriptExpression("'AAAA ' + $строка.getClass() + ' BBBB';");
		Value v = se.eval(new Row());
		System.out.println(v);
		System.out.println(v.getValue());
	}
	
	@Test
	//@Ignore
	public void testAdd()
	{
		TestInfinityGoodsSource goods = new TestInfinityGoodsSource();
		FluxDataSet fds = new FluxDataSet(goods.fetch().flux().take(100L));
		SelectOperation op = new SelectOperation(fds);
		op
			.rename("КОД", "CODE")
			.add(new StringType(), "Доб", new ScriptExpression("'Доб' + $строка.getByName('CODE').getValue()"))
			.execute()
				.flux()
					.take(5)
					.subscribe(System.out::println);
	}
}
