package test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import pdc2.core.dataset.AbstractDataSet;
import pdc2.core.dataset.FileDataSet;
import pdc2.core.dataset.FluxDataSet;
import pdc2.core.dataset.Row;
import pdc2.core.expression.ColumnNameExpression;
import pdc2.core.operation.JoinOperation;
import pdc2.core.source.TestInfinityBarcodesSource;
import pdc2.core.source.TestInfinityGoodsSource;
import reactor.core.publisher.Flux;

public class JoinOperationTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@Test
	@Ignore
	public void testInnerJoin()
	{
		ConcurrentHashMap<Comparable, List<Row>> joinIndex = new ConcurrentHashMap<>();
		TestInfinityGoodsSource goodsSource = new TestInfinityGoodsSource();
		TestInfinityBarcodesSource barcodesSource = new TestInfinityBarcodesSource();
		goodsSource.fetch()
			.flux()			
			.take(10)
			.groupBy( row -> {return row.getByName("КОД").getValue();})
			.parallel()
			.subscribe( gf -> {
				gf.subscribe( row -> {
					if (!joinIndex.containsKey(gf.key()))
					{
						joinIndex.put(gf.key(), new ArrayList<>());
					}
					joinIndex.get(gf.key()).add(row);
				});
			});
		barcodesSource.fetch().flux().filter( row -> {
			return joinIndex.containsKey(row.getByName("КОД").getValue());
		}).flatMap( rightRow -> {
			List<Row> resList = new ArrayList<>();
			
			joinIndex.get(rightRow.getByName("КОД").getValue()).forEach( leftRow -> {
				Row newRow = new Row();
				leftRow.getValues().forEach(newRow.getValues()::add);
				rightRow.getValues().forEach(newRow.getValues()::add);
				resList.add(newRow);
			});
			
			return Flux.fromIterable(resList);
		})
		//.take(5)
		//.subscribe();		
		.subscribe(System.out::println);
	}
	
	@Test
	@Ignore
	public void testInnerJoin2()
	{
		TestInfinityGoodsSource goodsSource = new TestInfinityGoodsSource();
		TestInfinityBarcodesSource barcodesSource = new TestInfinityBarcodesSource();
		JoinOperation op = new JoinOperation();
		op.setLeft(new FluxDataSet(goodsSource.fetch().flux().take(10)));
		op.setLeftExpression(new ColumnNameExpression("КОД"));
		op.setRight(new FluxDataSet(barcodesSource.fetch().flux().take(10)));
		op.setRightExpression(new ColumnNameExpression("КОД"));
		op.setInnerJoinMode(true);		
		AbstractDataSet ds = op.execute();
		ds.flux()
			.take(5)			
			.subscribe(System.out::println);
		System.out.println("-------------");
		ds.flux()
			.take(10)			
			.subscribe(System.out::println);
		//ShowOperation show = new ShowOperation(ds);
		//show.execute();
	}
	
	@Test
	//@Ignore
	public void testLeftJoin()
	{		
		TestInfinityGoodsSource goodsSource = new TestInfinityGoodsSource();
		TestInfinityBarcodesSource barcodesSource = new TestInfinityBarcodesSource();
		JoinOperation op = new JoinOperation();
		op.setLeft(new FluxDataSet(goodsSource.fetch().flux().take(10)));
		op.setLeftExpression(new ColumnNameExpression("КОД"));
		op.setRight(new FluxDataSet(barcodesSource.fetch().flux().take(10)));
		op.setRightExpression(new ColumnNameExpression("КОД"));
		op.setInnerJoinMode(false);		
		AbstractDataSet ds = op.execute();
		ds.flux()
			.filter( row -> {return row.getByName("ШТРИХКОД").getValue() != null;})
			.take(5)			
			.subscribe(System.out::println);		
	}	
}
