package pdc2.core.operation;

import java.util.function.Consumer;

import pdc2.core.dataset.AbstractDataSet;

public class ShowOperation implements Operation 
{
	private AbstractDataSet dataSet;
	
	private Consumer<AbstractDataSet> onCompleteListener;
	
	@Override
	public void registerOnCompleteListener(Consumer<AbstractDataSet> onCompleteListener) 
	{
		this.onCompleteListener = onCompleteListener;
	}
		
	public ShowOperation(AbstractDataSet dataSet) 
	{
		super();
		this.dataSet = dataSet;
	}

	@Override
	public AbstractDataSet execute() 
	{
		System.out.println(dataSet);
		if (this.onCompleteListener != null)
		{
			this.onCompleteListener.accept(dataSet);
		}
		return dataSet;
	}	
}
