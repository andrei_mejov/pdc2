package pdc2.core.operation;

import java.util.function.Consumer;

import pdc2.core.dataset.AbstractDataSet;

public interface Operation 
{
	public AbstractDataSet execute();
	public void registerOnCompleteListener(Consumer<AbstractDataSet> onCompleteListener);
}
