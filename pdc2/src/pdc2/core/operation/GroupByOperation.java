package pdc2.core.operation;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

import pdc2.core.dataset.AbstractDataSet;
import pdc2.core.dataset.FluxDataSet;
import pdc2.core.dataset.Row;
import pdc2.core.dataset.Value;
import reactor.core.publisher.Flux;
import reactor.core.publisher.GroupedFlux;

public class GroupByOperation implements Operation 
{
	private AbstractDataSet dataSet;
	private List<String> groupColumns = new ArrayList<>();
	private List<String> sumColumns = new ArrayList<>();
	private List<String> maxColumns = new ArrayList<>();
	private List<String> minColumns = new ArrayList<>();
	
	private Consumer<AbstractDataSet> onCompleteListener;
		
	public List<String> getGroupColumns() 
	{
		return groupColumns;
	}
	public void setGroupColumns(List<String> groupColumns) 
	{
		this.groupColumns = groupColumns;
	}

	public List<String> getSumColumns() 
	{
		return sumColumns;
	}
	public void setSumColumns(List<String> sumColumns) 
	{
		this.sumColumns = sumColumns;
	}

	public List<String> getMaxColumns() 
	{
		return maxColumns;
	}
	public void setMaxColumns(List<String> maxColumns) 
	{
		this.maxColumns = maxColumns;
	}

	public List<String> getMinColumns() 
	{
		return minColumns;
	}
	public void setMinColumns(List<String> minColumns) 
	{
		this.minColumns = minColumns;
	}
	
	public GroupByOperation from(AbstractDataSet dataSet)
	{
		this.dataSet = dataSet;
		return this;
	}
	
	public GroupByOperation columns(String ...columns)
	{
		for (String c : columns)
		{
			groupColumns.add(c);
		}
		return this;
	}
	
	public GroupByOperation sum(String ...columns)
	{
		for (String c : columns)
		{
			sumColumns.add(c);
		}
		return this;
	}
	
	public GroupByOperation max(String ...columns)
	{
		for (String c : columns)
		{
			maxColumns.add(c);
		}
		return this;
	}
	
	public GroupByOperation min(String ...columns)
	{
		for (String c : columns)
		{
			minColumns.add(c);
		}
		return this;
	}

	@Override
	public void registerOnCompleteListener(Consumer<AbstractDataSet> onCompleteListener) 
	{
		this.onCompleteListener = onCompleteListener;
	}

	@Override
	public AbstractDataSet execute() 
	{
		Flux<Row> flux = dataSet.flux().groupBy( row -> {			
			String key = null;
			if (groupColumns.size() > 1)
			{
				key = groupColumns.stream().reduce( (a, b) -> {								
					return "" + row.getByName(a).getValue() + row.getByName(b).getValue();
				}).get();
			}
			else
			{
				key = "" + row.getByName(groupColumns.get(0)).getValue();
			}			
			return key;
		}).map( grouppedFlux -> {
			final Row resultRow = new Row();		
			AtomicBoolean isFirst = new AtomicBoolean(true);
			grouppedFlux.subscribe(row -> {				
				if (row.getValues().size() != 
						groupColumns.size() + sumColumns.size() + maxColumns.size() + minColumns.size())
				{
					throw new RuntimeException("Wrong coumns count in: " + row);
				}
				if (isFirst.get())
				{
					row.getValues().forEach( v -> {
						resultRow.getValues().add(v.copy());
					});					
					isFirst.set(false);
				}
				else
				{
					sumColumns.forEach( c -> {
						Value v = resultRow.getByName(c);
						v.getType().add(v, row.getByName(c));						
					});
					maxColumns.forEach( c -> {
						Value v = resultRow.getByName(c);
						v.getType().max(v, row.getByName(c));						
					});
					minColumns.forEach( c -> {
						Value v = resultRow.getByName(c);
						v.getType().min(v, row.getByName(c));						
					});
				}
			});
			
			return resultRow;
		});
		FluxDataSet fds = new FluxDataSet(flux);
		if (this.onCompleteListener != null)
		{
			this.onCompleteListener.accept(fds);
		}
		return fds;		
	}		
}
