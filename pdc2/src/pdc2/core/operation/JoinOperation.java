package pdc2.core.operation;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import pdc2.core.dataset.AbstractDataSet;
import pdc2.core.dataset.FluxDataSet;
import pdc2.core.dataset.Index;
import pdc2.core.dataset.Row;
import pdc2.core.expression.ColumnNameExpression;
import reactor.core.publisher.Flux;

public class JoinOperation implements Operation
{
	private AbstractDataSet left;
	private AbstractDataSet right;
	
	private Boolean innerJoinMode = false;
	private ColumnNameExpression leftExpression;
	private ColumnNameExpression rightExpression;
	
	private Consumer<AbstractDataSet> onCompleteListener;
	
	@Override
	public void registerOnCompleteListener(Consumer<AbstractDataSet> onCompleteListener) 
	{
		this.onCompleteListener = onCompleteListener;
	}
	
	public AbstractDataSet getLeft() 
	{
		return left;
	}
	public void setLeft(AbstractDataSet left) 
	{
		this.left = left;
	}
	
	public JoinOperation left(AbstractDataSet left)
	{
		setLeft(left);
		return this;
	}

	public AbstractDataSet getRight() 
	{
		return right;
	}
	public void setRight(AbstractDataSet right) 
	{
		this.right = right;
	}
	
	public JoinOperation right(AbstractDataSet right)
	{
		setRight(right);
		return this;
	}

	public Boolean getInnerJoinMode() 
	{
		return innerJoinMode;
	}
	public void setInnerJoinMode(Boolean innerJoinMode) 
	{
		this.innerJoinMode = innerJoinMode;
	}
	
	public JoinOperation innerJoin()
	{
		setInnerJoinMode(true);
		return this;
	}

	public ColumnNameExpression getLeftExpression() 
	{
		return leftExpression;
	}
	
	public void setLeftExpression(ColumnNameExpression leftExpression) 
	{
		this.leftExpression = leftExpression;
	}
	
	public JoinOperation leftExpression(String leftColumnName)
	{
		setLeftExpression(new ColumnNameExpression(leftColumnName));
		return this;
	}
	
	public ColumnNameExpression getRightExpression() 
	{
		return rightExpression;
	}
	
	public void setRightExpression(ColumnNameExpression rightExpression) 
	{
		this.rightExpression = rightExpression;
	}
	
	public JoinOperation rightExpression(String columnName)
	{
		setRightExpression(new ColumnNameExpression(columnName));
		return this;
	}
	
	@Override
	public AbstractDataSet execute() 
	{		
		if (innerJoinMode)
		{			
			Index index = left.needIndex(leftExpression.getColumnName());
			Flux<Row> res = right.flux().filter( row -> {
				return index.getIndex().containsKey(rightExpression.eval(row).getValue());						
			}).flatMap( rightRow -> {
				List<Row> resList = index.eq(rightExpression.eval(rightRow)).parallelStream().map( leftRow -> {
					Row newRow = new Row();
					leftRow.getValues().stream().map(row -> row.copy()).forEach(newRow.getValues()::add);
					rightRow.getValues().stream().map(row -> row.copy()).forEach(newRow.getValues()::add);
					return newRow;
				}).collect(Collectors.toList());
				return Flux.fromIterable(resList);
			}).map( row -> {				
				row.ensureUniqColumnNames();
				return row;
			});
			FluxDataSet fds = new FluxDataSet(res);
			if (this.onCompleteListener != null)
			{
				this.onCompleteListener.accept(fds);
			}
			return fds;			
		}
		else
		{			
			Index index = right.needIndex(rightExpression.getColumnName());
			Row firstRightRow = null;
			if (index.getIndex().size() > 0)
			{
				firstRightRow = index.getIndex().values().stream().findFirst().get().get(0);				
			}
			final Row finalFirstRightRow = firstRightRow;
			
			Flux<Row> res = left.flux().flatMap( leftRow -> {
				List<Row> resList = null;
				
				if (index.getIndex().containsKey(leftExpression.eval(leftRow).getValue()))						
				{					
					resList = index.eq(leftExpression.eval(leftRow)).parallelStream().map( rightRow -> {
						Row newRow = new Row();
						leftRow.getValues().stream().map(row -> row.copy()).forEach(newRow.getValues()::add);
						rightRow.getValues().stream().map(row -> row.copy()).forEach(newRow.getValues()::add);
						return newRow;
					}).collect(Collectors.toList());
				}
				else
				{										
					Row newRow = new Row();
					leftRow.getValues().forEach(newRow.getValues()::add);
					if (finalFirstRightRow != null)
					{
						finalFirstRightRow.getValues().stream().map(row -> row.copy()).forEach( v -> {
							newRow.getValues().add(v.getType().fromString(v.getName(), "<NULL>"));
						});
					}
					resList = new ArrayList<>();
					resList.add(newRow);			
				}
				
				return Flux.fromIterable(resList);
			});			
			res = res.map( row -> {				
				row.ensureUniqColumnNames();
				return row;
			});
			FluxDataSet fds = new FluxDataSet(res);
			if (this.onCompleteListener != null)
			{
				this.onCompleteListener.accept(fds);
			}
			return fds;
		}
	}	
}
