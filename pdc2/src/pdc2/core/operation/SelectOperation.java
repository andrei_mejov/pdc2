package pdc2.core.operation;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

import pdc2.core.dataset.AbstractDataSet;
import pdc2.core.dataset.FluxDataSet;
import pdc2.core.dataset.Row;
import pdc2.core.dataset.Value;
import pdc2.core.expression.BooleanExpression;
import pdc2.core.expression.Expression;
import pdc2.core.type.AbstractType;
import reactor.core.publisher.Flux;

public class SelectOperation implements Operation 
{
	public static class OrderByColumn
	{
		private String name;
		private Boolean desc;
		
		public OrderByColumn(String name, Boolean desc) 
		{
			super();
			this.name = name;
			this.desc = desc;
		}
		
		public String getName() 
		{
			return name;
		}
		public void setName(String name) 
		{
			this.name = name;
		}
		
		public Boolean getDesc() 
		{
			return desc;
		}
		public void setDesc(Boolean desc) 
		{
			this.desc = desc;
		}
	}
	
	private AbstractDataSet fromDataSet;
	private Flux<Row> operationFlux = null;
	
	private Consumer<AbstractDataSet> onCompleteListener;
	
	@Override
	public void registerOnCompleteListener(Consumer<AbstractDataSet> onCompleteListener) 
	{
		this.onCompleteListener = onCompleteListener;
	}
			
	public Flux<Row> getOperationFlux() 
	{
		return operationFlux;
	}

	public SelectOperation(AbstractDataSet fromDataSet) 
	{
		super();
		this.fromDataSet = fromDataSet;
		this.operationFlux = this.fromDataSet.flux();
	}
	
	public SelectOperation add(AbstractType type, String name, Expression expression)
	{
		operationFlux = operationFlux.map( row -> {
			Value v = expression.eval(row);
			if (!v.getType().getClass().equals(type.getClass()) && v.getValue() != null)
			{
				throw new RuntimeException("Value type mismatch: " + type + " != " + v.getType());
			}
			v.setName(name.toUpperCase());
			v.setType(type);			
			row.getValues().add(v);
			return row;
		});
		return this;
	}
	
	public SelectOperation rename(String oldName, String newName)
	{		
		operationFlux = operationFlux.map( row -> {			
			row.getByName(oldName).setName(newName.toUpperCase());			
			return row;
		});
		return this;
	}
	
	public SelectOperation project(String ...names)
	{
		final List<Integer> indexes = new ArrayList<>();
		operationFlux = operationFlux.map( row -> {
			if (indexes.isEmpty())
			{
				for (String name : names)
				{
					for (Integer i = 0; i < row.getValues().size(); i++)
					{
						if (row.getValues().get(i).getName().equals(name))
						{
							indexes.add(i);
						}
					}
				}
			}
			Integer[] ia = new Integer[indexes.size()];
			indexes.toArray(ia);
			return row.project(ia);
		});
		return this;
	}
	
	public SelectOperation cast(String name, AbstractType type)
	{		
		final AtomicInteger index = new AtomicInteger(-1);
		operationFlux = operationFlux.map( row -> {
			if (index.get() == -1)
			{
				for (Integer i = 0; i < row.getValues().size(); i++)
				{
					if (row.getValues().get(i).getName().equals(name))
					{
						index.set(i);
					}
				}
			}			
			Value v = row.getValues().get(index.get());						
			v.setValue(type.fromString("", v.getType().toString(v)).getValue());
			v.setType(type);
			return row;
		});
		return this;
	}
	
	public SelectOperation where(Predicate<Row> cond)
	{
		operationFlux = operationFlux.filter(cond);
		return this;
	}
	
	public SelectOperation where(BooleanExpression be)
	{
		operationFlux = operationFlux.filter( row -> {
			return (Boolean)be.eval(row).getValue();
		});
		return this;
	}
	
	public SelectOperation orderBy(String ...names)
	{
		List<OrderByColumn> columnsList = new ArrayList<>();
		for (String name : names)
		{
			columnsList.add(new OrderByColumn(name, false));
		}
		OrderByColumn[] oa = new OrderByColumn[columnsList.size()];
		columnsList.toArray(oa);
		this.orderBy(oa);
		return this;
	}
	
	public SelectOperation orderByDesc(String ...names)
	{
		List<OrderByColumn> columnsList = new ArrayList<>();
		for (String name : names)
		{
			columnsList.add(new OrderByColumn(name, true));
		}
		OrderByColumn[] oa = new OrderByColumn[columnsList.size()];
		columnsList.toArray(oa);
		this.orderBy(oa);
		return this;
	}
	
	public SelectOperation orderBy(OrderByColumn ...columns)
	{
		operationFlux = operationFlux.sort((a, b) -> {
			Optional<Integer> o = Stream.of(columns).sequential().map( c -> {
				Integer m = c.getDesc() ? -1 : 1;
				return a.getByName(c.getName()).compareTo(b.getByName(c.getName())) * m;
			}).filter( comp -> !comp.equals(0)).findFirst();
			return o.orElse(0);
		});
		return this;
	}

	public SelectOperation union(Flux<Row> otherFlux)
	{
		operationFlux = Flux.concat(operationFlux, otherFlux);
		return this;
	}
	
	public SelectOperation first(Integer count)
	{
		operationFlux = operationFlux.take(count);
		return this;
	}

	public AbstractDataSet execute()
	{
		operationFlux = operationFlux.map( row -> {				
			row.ensureUniqColumnNames();
			return row;
		});
		FluxDataSet fds = new FluxDataSet(operationFlux);
		if (this.onCompleteListener != null)
		{
			this.onCompleteListener.accept(fds);
		}
		return fds;
	}
}
