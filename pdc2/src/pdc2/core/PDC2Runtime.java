package pdc2.core;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.antlr.v4.runtime.ANTLRErrorListener;
import org.antlr.v4.runtime.ANTLRErrorStrategy;
import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.atn.ATNConfigSet;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.ParseCancellationException;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import pdc2.core.antlr.PDC2Lexer;
import pdc2.core.antlr.PDC2Parser;
import pdc2.core.consumer.PDC2Consumer;
import pdc2.core.consumer.StdOutConsumer;
import pdc2.core.dataset.AbstractDataSet;
import pdc2.core.dataset.FileDataSet;
import pdc2.core.operation.GroupByOperation;
import pdc2.core.operation.JoinOperation;
import pdc2.core.operation.Operation;
import pdc2.core.operation.SelectOperation;
import pdc2.core.source.PDC2Source;
import pdc2.core.type.AbstractType;

public class PDC2Runtime 
{
	private List<Operation> operations = new ArrayList<>();
	
	private ConcurrentHashMap<String, AbstractDataSet> dataSets
		= new ConcurrentHashMap<>();
	private ConcurrentHashMap<String, PDC2Source> sources
		= new ConcurrentHashMap<>();
	private ConcurrentHashMap<String, PDC2Consumer> consumers
		= new ConcurrentHashMap<>();
	
	private AbstractDataSet returnedDataSet;	
			
	public AbstractDataSet getReturnedDataSet() 
	{
		return returnedDataSet;
	}

	public ConcurrentHashMap<String, AbstractDataSet> getDataSets() 
	{
		return dataSets;
	}

	public ConcurrentHashMap<String, PDC2Source> getSources() 
	{
		return sources;
	}

	public void show(String dataSetName)
	{
		StdOutConsumer c = new StdOutConsumer();
		c.init(new HashMap<>());
		c.accept(dataSets.get(dataSetName));		
	}
	
	public void saveWithConsumer(String dataSetName, String consumerName)
	{
		AbstractDataSet ds = dataSets.get(dataSetName);
		if (ds == null)
		{
			throw new RuntimeException("Data set or source not found: " + dataSetName);
		}
		PDC2Consumer consumer = consumers.get(consumerName);
		if (consumer == null) 
		{
			throw new RuntimeException("Consumer not found: " + consumerName);
		}
		consumer.accept(ds);
	}
	
	public void save(String dataSetName, String fileName)
	{
		AbstractDataSet ds = dataSets.get(dataSetName);
		if (ds == null)
		{
			throw new RuntimeException("Data set or source not found: " + dataSetName);
		}
		ds.save(fileName);		
	}
	
	public void defineConsumer(String consumerName, HashMap<String, String> parameters)
	{
		try
		{
			Class<? extends PDC2Consumer> clazz =
					(Class<? extends PDC2Consumer>)Class.forName(parameters.get("class"));
			PDC2Consumer consumer = clazz.newInstance();
			consumer.init(parameters);
			consumers.put(consumerName, consumer);
		}
		catch (Throwable t)
		{
			throw new RuntimeException(t);
		}
	}
	
	public void defineSource(String sourceName, HashMap<String, String> parameters)
	{
		try
		{
			Class<? extends PDC2Source> clazz =
					(Class<? extends PDC2Source>)Class.forName(parameters.get("class"));
			PDC2Source source = clazz.newInstance();
			source.init(parameters);
			sources.put(sourceName, source);
		}
		catch (Throwable t)
		{
			throw new RuntimeException(t);
		}
	}
		
	public SelectOperation select(String from, String intoDataSet)
	{
		AbstractDataSet fromDataSet = dataSets.get(from);
		if (fromDataSet == null)
		{
			if (sources.containsKey(from))
			{
				fromDataSet = sources.get(from).fetch();
			}
		}
		if (fromDataSet == null)
		{
			throw new RuntimeException("Data set or source not found: " + from);
		}
		SelectOperation op = new SelectOperation(fromDataSet);
		op.registerOnCompleteListener( ds -> {
			dataSets.put(intoDataSet, ds);
		});
		return op;
	}
	
	public SelectOperation selectFromFile(String fileName, String intoDataSet)
	{
		AbstractDataSet fromDataSet = new FileDataSet(fileName);
		SelectOperation op = new SelectOperation(fromDataSet);
		op.registerOnCompleteListener( ds -> {
			dataSets.put(intoDataSet, ds);
		});
		return op;
	}
	
	public JoinOperation join(String intoDataSet)
	{
		JoinOperation op = new JoinOperation();
		op.registerOnCompleteListener( ds -> {
			dataSets.put(intoDataSet, ds);
		});
		return op;
	}
	
	public GroupByOperation groupBy(String intoDataSet)
	{		
		GroupByOperation op = new GroupByOperation();
		op.registerOnCompleteListener( ds -> {
			dataSets.put(intoDataSet, ds);
		});
		return op;		
	}
	
	public void ret(String dataSetName)
	{
		AbstractDataSet ds = dataSets.get(dataSetName);
		if (ds == null)
		{
			throw new RuntimeException("Data set not found: " + dataSetName);
		}
		returnedDataSet = ds;
	}
	
	public void exec(String scriptText)
	{
		try
		{
			ScriptEngine engine = new ScriptEngineManager()
				.getEngineByName("nashorn");
			engine.getBindings(ScriptContext.ENGINE_SCOPE)
				.put("$среда", this);
			engine.getBindings(ScriptContext.ENGINE_SCOPE)
				.put("$", new ScriptUtil());
			engine.eval(scriptText);
		}
		catch (ScriptException se)
		{
			throw new RuntimeException(se);
		}
	}
	
	public void eval(File file, HashMap<String, String> parameters)
	{
		try
		{
			try (FileInputStream fis = new FileInputStream(file))
			{
				byte[] buf = new byte[fis.available()];
				fis.read(buf, 0, buf.length);
				eval(new String(buf, "UTF-8"), parameters);
			}
		}
		catch (Throwable t)
		{
			throw new RuntimeException(t);
		}
	}
	
	public void eval(String s, HashMap<String, String> parameters)
	{
		try
		{			
			String text = s;
			for (String name : parameters.keySet())
			{
				text = text.replace("${" + name + "}", parameters.get(name));
			}
			byte[] buf = text.getBytes("UTF-8");
			PDC2Lexer lexer = new PDC2Lexer(
				CharStreams.fromStream(new ByteArrayInputStream(buf), Charset.forName("UTF-8"))
			);		
			PDC2Parser parser = new PDC2Parser(new CommonTokenStream(lexer));
			
			parser.addErrorListener(new BaseErrorListener() {
				@Override
				public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line,
						int charPositionInLine, String msg, RecognitionException e) {
					 throw new ParseCancellationException(
						 "" + line + ":" + charPositionInLine + " " + msg
					 );
				}				
			});
			
			ParseTreeWalker.DEFAULT.walk(new PDC2RuntimeListener(this, parameters) , parser.r());
		}
		catch (Throwable t)
		{
			throw new RuntimeException(t);
		}
	}
}
