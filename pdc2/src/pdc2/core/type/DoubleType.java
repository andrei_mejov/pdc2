package pdc2.core.type;

import pdc2.core.dataset.ColumnMetaData;
import pdc2.core.dataset.Value;

public class DoubleType extends AbstractType
{
	@Override
	public Value wrap(String name, Comparable o) 
	{
		if (!(o instanceof Double))
		{
			throw new RuntimeException("Type mismatch: " + o.getClass() + " != Double");
		}
		Value v = new Value();
		v.setName(name);
		v.setType(this);
		v.setValue(o);
		return v;
	}

	@Override
	public Value fromString(String name, String s) 
	{
		Value v = new Value();
		v.setName(name);		
		v.setType(this);
		if (s.equals("<NULL>"))
		{
			v.setValue(null);
		}
		else
		{
			v.setValue(Double.parseDouble(s.trim().replace(",", ".")));			
		}
		return v;
	}
		
	@Override
	public void add(Value a, Value b) 
	{	
		a.setValue((Double)a.getValue() + (Double)b.getValue());
	}		

	@Override
	public String toString(Value v) 
	{	
		Object o = v.getValue();
		if (o == null)
		{
			return "<NULL>";			
		}
		else
		{
			return o.toString();
		}		
	}

	@Override
	public String toCharDescription() 
	{	
		return "D";
	}
}
