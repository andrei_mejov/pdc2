package pdc2.core.type;

import java.text.SimpleDateFormat;
import java.util.Date;

import pdc2.core.dataset.ColumnMetaData;
import pdc2.core.dataset.Value;

public class TimestampType extends AbstractType 
{
	private SimpleDateFormat createFormat()
	{
		return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
	}
	
	@Override
	public Value wrap(String name, Comparable o) 
	{	
		if (!(o instanceof Date))
		{
			throw new RuntimeException("Type mismatch: " + o.getClass() + " != Date");
		}
		Value v = new Value();
		v.setName(name);		
		v.setType(this);
		v.setValue(o);
		return v;
	}

	@Override
	public Value fromString(String name, String s) 
	{
		Value v = new Value();
		v.setName(name);		
		v.setType(this);
		if (s.equals("<NULL>"))
		{
			v.setValue(null);
		}
		else
		{
			try
			{
				v.setValue(createFormat().parse(s));
			}
			catch (Throwable t)
			{
				throw new RuntimeException(t);
			}
		}
		return v;
	}
	
	@Override
	public void add(Value a, Value b) 
	{	
		throw new RuntimeException("Unsupported operation for timestamp type");
	}

	@Override
	public String toString(Value v) 
	{	
		Object o = v.getValue();
		if (o == null)
		{
			return "<NULL>";			
		}
		else
		{
			return createFormat().format((Date)v.getValue());
		}		
	}

	@Override
	public String toCharDescription() 
	{	
		return "T";
	}
}
