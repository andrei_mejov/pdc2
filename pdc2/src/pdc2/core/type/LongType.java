package pdc2.core.type;

import pdc2.core.dataset.ColumnMetaData;
import pdc2.core.dataset.Value;

public class LongType extends AbstractType 
{
	@Override
	public Value wrap(String name, Comparable o) 
	{
		if (!(o instanceof Long) && !(o instanceof Integer))
		{
			throw new RuntimeException("Type mismatch: " + o.getClass() + " != Long");
		}
		Value v = new Value();
		v.setName(name);		
		v.setType(this);
		if (o instanceof Integer)
		{
		 v.setValue(((Integer)o).longValue());	
		}
		else
		{
			v.setValue(o);
		}
		
		return v;
	}

	@Override
	public Value fromString(String name, String s) 
	{
		Value v = new Value();
		v.setName(name);		
		v.setType(this);
		if (s.equals("<NULL>"))
		{
			v.setValue(null);
		}
		else
		{
			v.setValue(Long.parseLong(s.trim()));			
		}
		return v;
	}
	
	@Override
	public void add(Value a, Value b) 
	{	
		a.setValue((Long)a.getValue() + (Long)b.getValue());
	}

	@Override
	public String toString(Value v) 
	{	
		Object o = v.getValue();
		if (o == null)
		{
			return "<NULL>";			
		}
		else
		{
			return o.toString();
		}		
	}

	@Override
	public String toCharDescription() 
	{	
		return "L";
	}
}
