package pdc2.core.type;

import pdc2.core.dataset.ColumnMetaData;
import pdc2.core.dataset.Value;

public class BooleanType extends AbstractType
{
	@Override
	public Value wrap(String name, Comparable o) 
	{	
		if (!(o instanceof Boolean))
		{
			throw new RuntimeException("Type mismatch: " + o.getClass() + " != Boolean");
		}
		Value v = new Value();
		v.setName(name);		
		v.setType(this);
		v.setValue(o);
		return v;
	}

	@Override
	public Value fromString(String name, String s) 
	{
		Value v = new Value();
		v.setName(name);		
		v.setType(this);
		if (s.equals("<NULL>"))
		{
			v.setValue(null);
		}
		else
		{
			if (s.trim().equals("истина"))
			{
				v.setValue(true);
			}
			else if (s.trim().equals("ложь"))
			{
				v.setValue(false);
			}
			else
			{
				v.setValue(Boolean.parseBoolean(s));
			}
		}
		return v;
	}
		
	@Override
	public void add(Value a, Value b) 
	{	
		throw new RuntimeException("Unsupported operation for boolean type");
	}			

	@Override
	public String toString(Value v) 
	{	
		Object o = v.getValue();
		if (o == null)
		{
			return "<NULL>";			
		}
		else
		{
			if ((Boolean)o)
			{
				return "истина";
			}
			else
			{
				return "ложь";
			}			
		}		
	}

	@Override
	public String toCharDescription() 
	{	
		return "B";
	}
}
