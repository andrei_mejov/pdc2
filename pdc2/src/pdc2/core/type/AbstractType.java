package pdc2.core.type;

import java.util.Date;

import pdc2.core.dataset.Value;

public abstract class AbstractType 
{	
	public Integer compare(Comparable a, Comparable b) 
	{			
		if (a == null && b != null)
		{
			return -1;
		}
		else if (a != null && b == null)
		{
			return 1;
		}
		else if (a == null && b == null)
		{
			return 0;
		}
		else
		{
			return a.compareTo(b);
		}		
	}
	
	public abstract Value wrap(String name, Comparable o);	
	public abstract Value fromString(String name, String s);
	public abstract String toString(Value v);
	public abstract String toCharDescription();
	public abstract void add(Value a, Value b);
		
	public void max(Value a, Value b) 
	{	
		Integer comp = a.compareTo(b);
		if (comp < 0)
		{
			a.setValue(b.getValue());
		}
	}
	
	public void min(Value a, Value b) 
	{	
		Integer comp = a.compareTo(b);
		if (comp > 0)
		{
			a.setValue(b.getValue());
		}
	}
	
	public static Value wrapValue(String name, Comparable o)
	{
		if (o instanceof String)
		{
			StringType st = new StringType();
			return st.wrap(name, o);			
		}
		else if ((o instanceof Long) || (o instanceof Integer)) 
		{
			LongType lt = new LongType();
			return lt.wrap(name, o);
		}
		else if (o instanceof Double)
		{
			DoubleType dt = new DoubleType();
			return dt.wrap(name, o);
		}
		else if (o instanceof Boolean)
		{
			BooleanType bt = new BooleanType();
			return bt.wrap(name, o);
		}
		else if (o instanceof Date)
		{
			TimestampType tt = new TimestampType();
			return tt.wrap(name, o);
		}
		else if (o == null)
		{
			StringType st = new StringType();
			return st.wrap(name, o);
		}
		else
		{
			throw new RuntimeException("Type not found for value: " + o + " class " + o.getClass());
		}
	}
	
	public static AbstractType findType(String typeChar)
	{
		if (typeChar.equals("S"))
		{
			return new StringType();
		}
		else if (typeChar.equals("L"))
		{
			return new LongType();
		}
		else if (typeChar.equals("D"))
		{
			return new DoubleType();
		}
		else if (typeChar.equals("B"))
		{
			return new BooleanType();
		}
		else if (typeChar.equals("T"))
		{
			return new TimestampType();
		}
		else
		{
			throw new RuntimeException("Type not found for char: " + typeChar);
		}
	}	
}
