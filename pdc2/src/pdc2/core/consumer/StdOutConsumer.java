package pdc2.core.consumer;

import java.util.HashMap;

import pdc2.core.dataset.AbstractDataSet;

public class StdOutConsumer implements PDC2Consumer 
{
	@Override
	public void init(HashMap<String, String> parameters) 
	{
	}

	@Override
	public void accept(AbstractDataSet dataSet) 
	{
		System.out.println(dataSet);
	}	
}
