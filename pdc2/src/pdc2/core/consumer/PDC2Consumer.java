package pdc2.core.consumer;

import java.util.HashMap;

import pdc2.core.dataset.AbstractDataSet;

public interface PDC2Consumer 
{
	public void init(HashMap<String, String> parameters);
	public void accept(AbstractDataSet dataSet);
}
