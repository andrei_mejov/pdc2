package pdc2.core.consumer;

import java.io.FileOutputStream;
import java.util.HashMap;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import pdc2.core.dataset.AbstractDataSet;
import pdc2.core.dataset.Row;
import pdc2.core.dataset.Value;

public class XLSXConsumer implements PDC2Consumer
{
	public static final String FILE = "file";
	
	private HashMap<String, String> parameters;
	
	@Override
	public void init(HashMap<String, String> parameters) 
	{
		this.parameters = parameters;
	}

	@Override
	public void accept(AbstractDataSet dataSet) 
	{
		String file = parameters.get(FILE);
		if (file == null)
		{
			throw new RuntimeException("Parameters not defined: " + FILE);
		}
		try
		{
			XSSFWorkbook workbook = new XSSFWorkbook();
			
			Sheet sheet = workbook.createSheet("Лист 1");
			
			Boolean isFirst = true;
			Integer rowIndex = 0;
			for (Row row : dataSet)
			{
				if (isFirst)
				{
					isFirst = false;
					org.apache.poi.ss.usermodel.Row xlsxRow = 
						sheet.createRow(rowIndex);
					for (Integer i = 0; i < row.getValues().size(); i++)
					{
						Value v = row.getValues().get(i);
						Cell xlsxCell = xlsxRow.createCell(i);
						xlsxCell.setCellValue("" + v.getName());
					}
					rowIndex++;
				}
				org.apache.poi.ss.usermodel.Row xlsxRow = 
					sheet.createRow(rowIndex);
				for (Integer i = 0; i < row.getValues().size(); i++)
				{
					Value v = row.getValues().get(i);
					Cell xlsxCell = xlsxRow.createCell(i);
					xlsxCell.setCellValue("" + v.getType().toString(v));
				}
				rowIndex++;
			}
			
			try (FileOutputStream fos = new FileOutputStream(file))
			{
				workbook.write(fos);
			}
			workbook.close();
		}
		catch (Throwable t)
		{
			throw new RuntimeException(t);
		}
	}
}
