package pdc2.core.consumer;

import java.util.HashMap;
import java.util.Map;

import com.lambdaworks.redis.RedisClient;
import com.lambdaworks.redis.RedisURI;
import com.lambdaworks.redis.api.StatefulRedisConnection;

import pdc2.core.dataset.AbstractDataSet;
import pdc2.core.dataset.ArrayDataSet;
import pdc2.core.dataset.Row;
import pdc2.core.dataset.Value;
import pdc2.core.type.AbstractType;

public class RedisHashConsumer implements PDC2Consumer
{
	public static final String URL = "url";
	public static final String KEYS_PREFIX = "keysPrefix";
	public static final String KEYS_COLUMN = "keysColumn";
	
	private HashMap<String, String> parameters;
	
	@Override
	public void init(HashMap<String, String> parameters) 
	{
		this.parameters = parameters;
	}

	@Override
	public void accept(AbstractDataSet dataSet) 
	{
		final ArrayDataSet ds = new ArrayDataSet();
		RedisClient redisClient = RedisClient.create(
			RedisURI.create(parameters.get(URL))
		);
		try
		{
			StatefulRedisConnection<String, String> connection = redisClient.connect();
			try
			{				
				dataSet.forEach( row -> {
					Value keyValue = row.getByName(parameters.get(KEYS_COLUMN));
					final HashMap<String, String> map = new HashMap<>();					
					String key = parameters
						.get(KEYS_PREFIX).replace("*", keyValue.getType().toString(keyValue));
					row.getValues().forEach( v -> {
						map.put(v.getName(), v.getType().toString(v));
					});
					connection.sync().hmset(key, map);
				});							
			}
			finally
			{
				connection.close();
			}
		}
		finally
		{
			redisClient.shutdown();
		}								
	}		
}
