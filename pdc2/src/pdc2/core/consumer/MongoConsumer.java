package pdc2.core.consumer;

import java.util.HashMap;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import pdc2.core.dataset.AbstractDataSet;

public class MongoConsumer implements PDC2Consumer 
{
	public static final String HOST = "host";
	public static final String PORT = "port";
	public static final String DATABASE = "database";
	public static final String COLLECTION = "collection";	
	
	private HashMap<String, String> parameters = new HashMap<>();
	
	@Override
	public void init(HashMap<String, String> parameters) 
	{
		this.parameters = parameters;
	}

	@Override
	public void accept(AbstractDataSet dataSet) 
	{
		MongoClient mongo = new MongoClient(
			parameters.get(HOST),
			Integer.parseInt(parameters.get(PORT))
		);
		try
		{
			MongoDatabase db = mongo.getDatabase(parameters.get(DATABASE));
			MongoCollection<Document> collection = 
				db.getCollection(parameters.get(COLLECTION));
			dataSet.forEach( row -> {
				Document doc = new Document();
				row.getValues().forEach( v -> {
					doc.append(v.getName(), v.getValue());
				});
				collection.insertOne(doc);
			});
		}
		finally
		{
			mongo.close();
		}
	}	
}
