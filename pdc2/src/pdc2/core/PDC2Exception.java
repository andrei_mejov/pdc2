package pdc2.core;

public class PDC2Exception extends RuntimeException
{
	public PDC2Exception(String message) 
	{
		super(message);	
	}	
}
