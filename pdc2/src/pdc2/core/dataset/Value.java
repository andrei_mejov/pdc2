package pdc2.core.dataset;

import java.io.Serializable;

import pdc2.core.type.AbstractType;

public class Value implements Serializable, Comparable<Value>
{
	private static final long serialVersionUID = 1L;
	
	private String name;
	private AbstractType type;
	private Comparable value;
		
	public String getName() 
	{
		return name;
	}
	public void setName(String name) 
	{
		this.name = name;
	}
	
	public AbstractType getType() 
	{
		return type;
	}
	public void setType(AbstractType type) 
	{
		this.type = type;
	}
	
	public Comparable getValue() 
	{
		return value;
	}
	public void setValue(Comparable value) 
	{
		this.value = value;
	}
	
	@Override
	public int compareTo(Value o) 
	{
		return this.getType().compare(this.getValue(), o.getValue());
	}
	
	public Value copy()
	{
		Value v = new Value();
		v.setName(name);
		v.setType(type);
		v.setValue(value);
		
		return v;
	}
}
