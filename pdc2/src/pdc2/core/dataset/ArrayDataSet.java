package pdc2.core.dataset;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import pdc2.core.type.AbstractType;
import reactor.core.publisher.Flux;

public class ArrayDataSet extends AbstractDataSet implements Serializable
{	
	private static final long serialVersionUID = 1L;		
		
	private List<Row> rows = new ArrayList<>();		
	
	private List<String> names = new ArrayList<>();
	private List<AbstractType> types = new ArrayList<>();
	
	public ArrayDataSet()
	{		
	}
	
	public ArrayDataSet(Flux<Row> flux)
	{
		rows = flux.toStream().collect(Collectors.toList());
	}
		
	public List<Row> getRows() 
	{
		return rows;
	}
	public void setRows(List<Row> rows) 
	{
		this.rows = rows;
	}
	
	public ArrayDataSet column(String name, AbstractType type)
	{
		names.add(name);
		types.add(type);		
		return this;
	}
	
	public ArrayDataSet row(Comparable ...values)
	{
		Row row = new Row();
		if (values.length != names.size())
		{
			throw new RuntimeException("Size of values list not equal columns amount");
		}
		for (Integer i = 0; i < values.length; i++)
		{
			Value v = types.get(i).wrapValue(names.get(i), values[i]);			
			row.getValues().add(v);
		}
		rows.add(row);
		return this;
	}
	
	@Override
	public Iterator<Row> iterator() 
	{
		return rows.iterator();
	}			
}
