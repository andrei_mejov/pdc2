package pdc2.core.dataset;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Row implements Serializable 
{
	private static final long serialVersionUID = 1L;		
	private List<Value> values = new ArrayList<>();		

	public List<Value> getValues() 
	{
		return values;
	}
	public void setValues(List<Value> values) 
	{
		this.values = values;
	}
	
	public Value getByName(String name)
	{		
		for (Value v : values)
		{
			if (v.getName().equals(name))
			{
				return v;
			}
		}
		throw new RuntimeException("Column not found: " + name);		
	}
	
	public Row project(Integer ...indexes)
	{
		Row newRow = new Row();
		for (Integer i : indexes)
		{
			newRow.getValues().add(this.getValues().get(i));
		}
		return newRow;
	}
	
	public void ensureUniqColumnNames()
	{
		HashMap<String, Integer> hash = new HashMap<>();		
		this.getValues().forEach( v -> {
			if (hash.containsKey(v.getName()))
			{
				Integer counter = hash.get(v.getName());
				do 
				{
					counter++;
				} while (hash.containsKey(v.getName() + counter));
				
				hash.put(v.getName(), counter);
				v.setName(v.getName() + counter);
				hash.put(v.getName(), 0);
			}
			else
			{
				hash.put(v.getName(), 0);
			}
		});
	}
	
	@Override
	public String toString() 
	{
		String s = "";
		for (Value v : values)
		{
			if (!s.equals(""))
			{
				s += ";";
			}
			s += v.getName() + "=" + v.getType().toString(v);
		}
		return s;
	}
	
	public String toJson()
	{
		StringBuilder sb = new  StringBuilder();	
		sb.append("{");	
		Boolean isFirst = true;
		for (Value v : values)
		{
			if (!isFirst)
			{
				sb.append(",");
			}
			isFirst = false;
			sb.append("\"" + v.getName() + "\"");
			sb.append(":");
			sb.append("\"" + v.getType().toString(v).replace("\"", "\\\"") + "\"");									
		}
		sb.append("}");
		return sb.toString();
	}
	
	public String toTypesJson()
	{
		StringBuilder sb = new  StringBuilder();	
		sb.append("{");	
		Boolean isFirst = true;
		for (Value v : values)
		{
			if (!isFirst)
			{
				sb.append(",");
			}
			isFirst = false;
			sb.append("\"" + v.getName() + "\"");
			sb.append(":");
			sb.append("\"" + v.getType().toCharDescription() + "\"");									
		}
		sb.append("}");
		return sb.toString();
	}
}
