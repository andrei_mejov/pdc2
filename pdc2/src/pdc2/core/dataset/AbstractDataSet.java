package pdc2.core.dataset;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import reactor.core.publisher.Flux;

public abstract class AbstractDataSet implements Iterable<Row> 
{	
	private ConcurrentHashMap<String, Index> indexes = new ConcurrentHashMap<>();
	
	public abstract Iterator<Row> iterator();	
	
	public Flux<Row> flux()
	{
		return Flux.fromIterable(this);
	}
			
	public Index needIndex(String columnName)
	{
		if (!indexes.containsKey(columnName))
		{
			buildIndex(columnName);
		}
		return indexes.get(columnName);
	}

	public void buildIndex(String columnName)
	{
		Index newIndex = new Index();
		this.flux()				
			.groupBy( row -> {return row.getByName(columnName).getValue();})
			.parallel()
			.subscribe( gf -> {
				gf.subscribe( row -> {
					if (!newIndex.getIndex().containsKey(gf.key()))
					{
						newIndex.getIndex().put(gf.key(), new ArrayList<>());
					}
					newIndex.getIndex().get(gf.key()).add(row);
				});
			});
		indexes.put(columnName, newIndex);
	}
	
	public void save(String fileName)
	{
		try
		{			
			try (BufferedWriter writer = new BufferedWriter(
					new OutputStreamWriter(new FileOutputStream(fileName), "UTF-8")
				))
			{
				Boolean isFirst = true;
				for (Row r : this)
				{					
					if (isFirst)
					{						
						String s = 
							String.join(";", 
								r.getValues().stream()
									.map( v -> {return v.getName();})
									.collect(Collectors.toList())
							) + "\n";						
						writer.write(s);
						s = String.join(";", 
							r.getValues().stream()
								.map( v -> {return v.getType().toCharDescription();})
								.collect(Collectors.toList())
							) + "\n";
						
						writer.write(s);
						
						isFirst = false;
					}
					
					String s = String.join(";", 
						r.getValues().stream()
							.map( v -> {
								return v.getType().toString(v);
							}).collect(Collectors.toList())
					) + "\n";
					writer.write(s);
					writer.flush();
				}//for
			}
		}
		catch (IOException e)
		{
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public String toString() 
	{
		StringBuilder sb = new StringBuilder();				
		Boolean isFirst = true;
		for (Row r : this)
		{
			if (isFirst)
			{
				sb.append(
						String.join(";", 
							r.getValues().stream()
								.map( v -> {return v.getName();})
								.collect(Collectors.toList())
						)
					);
					sb.append("\n");
					sb.append(
						String.join(";", 
							r.getValues().stream()
								.map( v -> {return v.getType().toCharDescription();})
								.collect(Collectors.toList())
						)
					);
				
				isFirst = false;
			}
			
			sb.append("\n");
			sb.append(
				String.join(";", 
					r.getValues().stream()
						.map( v -> {
							return v.getType().toString(v);
						}).collect(Collectors.toList())
				)
			);
		}
		return sb.toString();		
	}
	
	public String toJson()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("[");

		Boolean isFirst = true;
		for (Row r : this)
		{
			if (isFirst)
			{
				sb.append(r.toTypesJson());				
			}
			isFirst = false;
			
			sb.append(",");						
			sb.append(r.toJson());
		}
		
		sb.append("]");
		return sb.toString();
	}
}
