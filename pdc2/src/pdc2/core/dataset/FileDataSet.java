package pdc2.core.dataset;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import pdc2.core.type.AbstractType;
import reactor.core.publisher.Flux;

public class FileDataSet extends AbstractDataSet
{
	private String fileName;
		
	public FileDataSet(String fileName) 
	{
		super();
		this.fileName = fileName;
	}

	@Override
	public Iterator<Row> iterator() 
	{
		return new Iterator<Row>() 
		{
			BufferedReader reader = null;
			private Row row = null;			
			private List<String> names = new ArrayList<>();
			private List<AbstractType> types = new ArrayList<>();			
			
			@Override
			public boolean hasNext() 
			{
				try
				{
					if (reader == null)
					{
						reader = 
							new BufferedReader(
								new InputStreamReader(
									new FileInputStream(fileName), 
									"UTF-8"
								)
							);						
						
						String line = reader.readLine();
						String[] sa = line.split(";");
						for (String s : sa)
						{
							names.add(s.toUpperCase());
						}
						
						line = reader.readLine();
						sa = line.split(";");						
						for (Integer i = 0; i < sa.length; i++)
						{
							types.add(AbstractType.findType(sa[i]));							
						}
						
						if (names.size() != types.size())
						{
							throw new RuntimeException(
								"Columns count != types count in file " + fileName
							);
						}
					}
					
					String line = reader.readLine();
					if (line == null)
					{						
						row = null;
						reader.close();
						names.clear();
						types.clear();
					}					
					else
					{						
						String[] sa = line.split(";");
						if (sa.length != names.size())
						{
							throw new RuntimeException("Size of values list not equal columns amount");
						}
						row = new Row();
						for (Integer i = 0; i < names.size(); i++)
						{
							Value v = types.get(i).fromString(names.get(i), sa[i]);
							row.getValues().add(v);
						}
					}
					
					return row != null;
				}
				catch (Throwable t)
				{
					throw new RuntimeException(t);
				}
			}

			@Override
			public Row next() 
			{				
				return row;
			}			
		};			
	}//public Iterator<Row> iterator();	
}
