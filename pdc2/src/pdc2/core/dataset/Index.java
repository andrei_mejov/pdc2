package pdc2.core.dataset;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class Index 
{
	private ConcurrentHashMap<Comparable, List<Row>> index = new ConcurrentHashMap<>();
			
	public ConcurrentHashMap<Comparable, List<Row>> getIndex() 
	{
		return index;
	}	

	public List<Row> eq(Value value)
	{
		if (index.containsKey(value.getValue()))
		{
			return index.get(value.getValue());
		}
		else
		{
			return new ArrayList<>();
		}
	}
}
