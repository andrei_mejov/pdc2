package pdc2.core.dataset;

import java.io.Serializable;

import pdc2.core.type.AbstractType;

public class ColumnMetaData implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	private String name;
	private AbstractType type;
		
	public ColumnMetaData(String name, AbstractType type) 
	{
		super();
		this.name = name;
		this.type = type;
	}
	
	public String getName() 
	{
		return name;
	}
	public void setName(String name) 
	{
		this.name = name;
	}
	
	public AbstractType getType() 
	{
		return type;
	}
	public void setType(AbstractType type) 
	{
		this.type = type;
	}	
	
	public ColumnMetaData copy()
	{
		return new ColumnMetaData(name, type);
	}
}
