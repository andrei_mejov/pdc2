package pdc2.core.dataset;

import pdc2.core.expression.AndExpression;
import pdc2.core.expression.BooleanExpression;
import pdc2.core.expression.EqExpression;
import pdc2.core.expression.Expression;
import pdc2.core.expression.GtExpression;
import pdc2.core.expression.LtExpression;
import pdc2.core.expression.NotExpression;
import pdc2.core.expression.OrExpression;

public class FilterBuilder 
{
	public interface Builder
	{
		public BooleanExpression build();
	}
	
	public interface BinaryBuilder extends Builder
	{
		public void setLeft(BooleanExpression e);
		public void setRight(BooleanExpression e);
		public BooleanExpression build();
	}
	
	public interface UnaryBuilder extends Builder
	{
		public void setExpression(BooleanExpression e);		
		public BooleanExpression build();
	}
	
	public static FilterBuilder begin()
	{
		return new FilterBuilder();
	}		
	
	private BooleanExpression expression;
	private Builder builder;
	
	public BooleanExpression end()
	{
		if (builder != null)
		{
			expression = builder.build();
		}		
		return expression;
	}
	
	public FilterBuilder eq(Expression leftExpression, Expression rightExpression)
	{		
		expression = new EqExpression(leftExpression, rightExpression);
		if (builder != null)
		{			
			if (builder instanceof BinaryBuilder)
			{
				((BinaryBuilder)builder).setRight(expression);
			}
			else
			{
				((UnaryBuilder)builder).setExpression(expression);
			}
			expression = builder.build();
		}
		return this;
	}
	
	public FilterBuilder gt(Expression leftExpression, Expression rightExpression)
	{
		expression = new GtExpression(leftExpression, rightExpression);
		if (builder != null)
		{			
			if (builder instanceof BinaryBuilder)
			{
				((BinaryBuilder)builder).setRight(expression);
			}
			else
			{
				((UnaryBuilder)builder).setExpression(expression);
			}
			expression = builder.build();
		}
		return this;
	}
	
	public FilterBuilder lt(Expression leftExpression, Expression rightExpression)
	{
		expression = new LtExpression(leftExpression, rightExpression);
		if (builder != null)
		{			
			if (builder instanceof BinaryBuilder)
			{
				((BinaryBuilder)builder).setRight(expression);
			}
			else
			{
				((UnaryBuilder)builder).setExpression(expression);
			}
			expression = builder.build();
		}
		return this;
	}
	
	public FilterBuilder block(BooleanExpression e)
	{
		if (builder != null)
		{			
			if (builder instanceof BinaryBuilder)
			{
				((BinaryBuilder)builder).setRight(e);
			}
			else
			{
				((UnaryBuilder)builder).setExpression(e);
			}
			expression = builder.build();
		} 
		else
		{
			/*UnaryBuilder ub = new UnaryBuilder() 
			{
				private BooleanExpression expression;
				
				@Override
				public void setExpression(BooleanExpression e) 
				{
					expression = e;
				}
				
				@Override
				public BooleanExpression build() 
				{			
					return expression;
				}
			};
			expression = e;			
			ub.setExpression(e);			
			builder = ub;*/
			expression = e;
			builder = null;
		}
		return this;
	}
	
	public FilterBuilder and()
	{
		BinaryBuilder bb = new BinaryBuilder() {			
			private BooleanExpression left;
			private BooleanExpression right;
			
			@Override
			public void setRight(BooleanExpression e) 
			{
				left = e;
			}
			
			@Override
			public void setLeft(BooleanExpression e) 
			{
				right = e;
			}
			
			@Override
			public BooleanExpression build() 
			{			
				return new AndExpression(left, right);
			}
		};
		bb.setLeft(expression);
		expression = null;
		builder = bb;
		
		return this;
	}
	
	public FilterBuilder or()
	{
		BinaryBuilder bb = new BinaryBuilder() {			
			private BooleanExpression left;
			private BooleanExpression right;
			
			@Override
			public void setRight(BooleanExpression e) 
			{
				left = e;
			}
			
			@Override
			public void setLeft(BooleanExpression e) 
			{
				right = e;
			}
			
			@Override
			public BooleanExpression build() 
			{			
				return new OrExpression(left, right);
			}
		};
		bb.setLeft(expression);
		expression = null;
		builder = bb;
		
		return this;
	}
	
	public FilterBuilder not()
	{		
		UnaryBuilder ub = new UnaryBuilder() 
		{
			private BooleanExpression expression;
			
			@Override
			public void setExpression(BooleanExpression e) 
			{
				expression = e;
			}
			
			@Override
			public BooleanExpression build() 
			{			
				return new NotExpression(expression);
			}
		};
		expression = null;
		builder = ub;
		return this;
	}
}
