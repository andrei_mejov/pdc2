package pdc2.core.dataset;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicBoolean;

import reactor.core.publisher.Flux;

public class FluxDataSet extends AbstractDataSet 
{
	private Flux<Row> internalFlux;
	private LinkedList<Row> cache = new LinkedList<>();
	private AtomicBoolean completeFlag = new AtomicBoolean(false);
		
	public FluxDataSet(Flux<Row> internalFlux) 
	{
		super();
		this.internalFlux = internalFlux.doOnComplete(() -> {
			completeFlag.set(true);
		});		
	}
	
	private void fillCache(Row row)
	{
		synchronized (cache) 
		{
			cache.add(row);
		}		
	}

	@Override
	public Iterator<Row> iterator() 
	{		
		if (completeFlag.get())
		{
			return cache.iterator();
		}
		else
		{
			return internalFlux.doOnNext(this::fillCache).toIterable().iterator();
		}
	}

	@Override
	public Flux<Row> flux() 
	{
		if (completeFlag.get())
		{
			return Flux.fromIterable(cache);
		}
		else
		{
			return internalFlux.doOnNext(this::fillCache);
		}
	}		
}
