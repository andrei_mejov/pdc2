package pdc2.core;

import java.io.File;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.function.Function;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.TerminalNodeImpl;
import org.antlr.v4.runtime.tree.Tree;

import pdc2.core.antlr.PDC2BaseListener;
import pdc2.core.antlr.PDC2Parser.AddContext;
import pdc2.core.antlr.PDC2Parser.AndContext;
import pdc2.core.antlr.PDC2Parser.BlockOperationContext;
import pdc2.core.antlr.PDC2Parser.BoolContext;
import pdc2.core.antlr.PDC2Parser.CastContext;
import pdc2.core.antlr.PDC2Parser.ColumnNameContext;
import pdc2.core.antlr.PDC2Parser.ConditionContext;
import pdc2.core.antlr.PDC2Parser.ConsumerContext;
import pdc2.core.antlr.PDC2Parser.DataSetNameContext;
import pdc2.core.antlr.PDC2Parser.ExecContext;
import pdc2.core.antlr.PDC2Parser.ExecValueContext;
import pdc2.core.antlr.PDC2Parser.FirstContext;
import pdc2.core.antlr.PDC2Parser.GroupByColumnsContext;
import pdc2.core.antlr.PDC2Parser.GroupByContext;
import pdc2.core.antlr.PDC2Parser.InnerJoinContext;
import pdc2.core.antlr.PDC2Parser.InvokeContext;
import pdc2.core.antlr.PDC2Parser.LeftJoinContext;
import pdc2.core.antlr.PDC2Parser.LetContext;
import pdc2.core.antlr.PDC2Parser.NotContext;
import pdc2.core.antlr.PDC2Parser.NullValueContext;
import pdc2.core.antlr.PDC2Parser.NumberContext;
import pdc2.core.antlr.PDC2Parser.OrContext;
import pdc2.core.antlr.PDC2Parser.OrderByColumnContext;
import pdc2.core.antlr.PDC2Parser.OrderByContext;
import pdc2.core.antlr.PDC2Parser.ProjectContext;
import pdc2.core.antlr.PDC2Parser.RenameContext;
import pdc2.core.antlr.PDC2Parser.RetContext;
import pdc2.core.antlr.PDC2Parser.SaveContext;
import pdc2.core.antlr.PDC2Parser.SelectContext;
import pdc2.core.antlr.PDC2Parser.ShowContext;
import pdc2.core.antlr.PDC2Parser.SourceContext;
import pdc2.core.antlr.PDC2Parser.SourceParamContext;
import pdc2.core.antlr.PDC2Parser.StringContext;
import pdc2.core.antlr.PDC2Parser.TimestampContext;
import pdc2.core.antlr.PDC2Parser.TypeNamesContext;
import pdc2.core.antlr.PDC2Parser.UnionContext;
import pdc2.core.antlr.PDC2Parser.UsingContext;
import pdc2.core.antlr.PDC2Parser.ValueContext;
import pdc2.core.antlr.PDC2Parser.WhereContext;
import pdc2.core.dataset.AbstractDataSet;
import pdc2.core.dataset.ArrayDataSet;
import pdc2.core.dataset.FilterBuilder;
import pdc2.core.dataset.Value;
import pdc2.core.expression.ColumnNameExpression;
import pdc2.core.expression.ConstantExpression;
import pdc2.core.expression.Expression;
import pdc2.core.expression.ScriptExpression;
import pdc2.core.operation.Operation;
import pdc2.core.operation.SelectOperation;
import pdc2.core.operation.SelectOperation.OrderByColumn;
import pdc2.core.source.PDC2Source;
import pdc2.core.type.AbstractType;
import pdc2.core.type.BooleanType;
import pdc2.core.type.DoubleType;
import pdc2.core.type.LongType;
import pdc2.core.type.StringType;
import pdc2.core.type.TimestampType;

public class PDC2RuntimeListener extends PDC2BaseListener 
{
	private PDC2Runtime runtime;
	private Operation currentOperation;	
	private String targetDataSetName;
	
	private HashMap<String, String> parameters;
	
	private Deque<FilterBuilder> filtersStack = new ArrayDeque<>();

	public AbstractDataSet dataSetResolver(String name)
	{
		AbstractDataSet ds = runtime.getDataSets().get(name);
		if (ds == null)
		{
			ds = runtime.getSources().get(name).fetch();
		}
		if (ds == null)
		{
			throw new PDC2Exception("Источник/таблца не определена: " + name);
		}
		return ds;
	}
	
	public AbstractType typeNameResolver(TypeNamesContext ctx)
	{
		String typeName = ctx.getText();
		if (typeName.equals("целое"))
		{
			return new LongType();
		}
		else if (typeName.equals("вещественное"))
		{
			return new DoubleType();
		}
		else if (typeName.equals("булево"))
		{
			return new BooleanType();
		}
		else if (typeName.equals("дата"))
		{
			return new TimestampType();
		}
		else if (typeName.equals("строка"))
		{
			return new StringType();
		}		
		else
		{
			throw new PDC2Exception("Тип не известен: " + typeName);
		}
	}
	
	public String rq(String s)
	{
		return s.replaceAll("^[\"']", "").replaceAll("[\"']$", "");
	}
	
	public ScriptExpression buildScriptExpression(ExecValueContext evc)
	{
		String code = evc.getText().replaceAll("^\\{\\%", "").replaceAll("\\%\\}$", "");
		ScriptExpression se = new ScriptExpression(code);
		return se;
	}
	
	public void processNthChild(
		Tree ctx, 
		Integer nth, 
		Class<? extends ParserRuleContext> clazz, 
		Consumer<ParserRuleContext> consumer
	)
	{
		if (ctx.getChild(nth).getClass().isAssignableFrom(clazz))
		{						
			consumer.accept((ParserRuleContext)ctx.getChild(nth));
		}
	}
	
	public <T> T processNthChild(
		Tree ctx, 
		Integer nth, 
		Class<? extends ParserRuleContext> clazz, 
		Function<ParserRuleContext, T> mapper
	)
	{
		if (ctx.getChild(nth).getClass().isAssignableFrom(clazz))
		{						
			return mapper.apply((ParserRuleContext)ctx.getChild(nth));
		}
		else
		{
			return null;
		}
	}
	
	public <T> Optional<T> extractNthChild(
		Tree ctx,
		Integer nth,
		Class<T> clazz		
	)
	{
		if (ctx.getChild(nth).getClass().isAssignableFrom(clazz))
		{	
			return Optional.of((T)ctx.getChild(nth));			
		}
		else
		{
			return Optional.ofNullable(null);
		}
	}
					
	@Override
	public void enterLet(LetContext ctx) 
	{		
		if (currentOperation != null)
		{
			throw new PDC2Exception("Не завершена предыдущая операция для:" + ctx.getText());
		}
		targetDataSetName = rq(ctx.getChild(0).getChild(0).getText());
	}

	@Override
	public void enterSelect(SelectContext ctx) 
	{		
		if (targetDataSetName == null)
		{
			throw new PDC2Exception("Не завершена предыдущая операция для:" + ctx.getText());
		}
				
		processNthChild(ctx, 2, StringContext.class, s -> {
			currentOperation = runtime.selectFromFile(
				rq(s.getChild(0).getText()), targetDataSetName
			);
		});
		processNthChild(ctx, 2, DataSetNameContext.class, d -> {
			currentOperation = runtime.select(
				rq(d.getText()), targetDataSetName
			);
		});		
	}
		
	@Override
	public void enterFirst(FirstContext ctx) 
	{		
		if (currentOperation == null || !(currentOperation instanceof SelectOperation))
		{
			throw new PDC2Exception("Недопустимое использование оператора: " + ctx.getText());
		}
		
		processNthChild(ctx, 1, NumberContext.class, n -> {
			Integer count = Integer.parseInt(n.getText().replace("_", ""));
			((SelectOperation)currentOperation).first(count);
		});		
	}
		
	@Override
	public void enterProject(ProjectContext ctx) 
	{
		if (currentOperation == null || !(currentOperation instanceof SelectOperation))
		{
			throw new PDC2Exception("Недопустимое использование оператора: " + ctx.getText());
		}		
		List<String> columnNames = new ArrayList<>();
		for (Integer i = 0; i < ctx.getChildCount(); i++)
		{
			extractNthChild(ctx, i, ColumnNameContext.class).ifPresent( c -> 
				columnNames.add(c.getText())
			);
		}
		String[] sa = new String[columnNames.size()];
		columnNames.toArray(sa);
		((SelectOperation)currentOperation).project(sa);
	}
		
	@Override
	public void enterAdd(AddContext ctx) 
	{
		if (currentOperation == null || !(currentOperation instanceof SelectOperation))
		{
			throw new PDC2Exception("Недопустимое использование оператора: " + ctx.getText());
		}
		Integer i = 1;		
		while (i < ctx.getChildCount())
		{			
			TypeNamesContext tnc = (TypeNamesContext)ctx.getChild(i);
			ValueContext vc = (ValueContext)ctx.getChild(i + 1);
			ColumnNameContext cnc = (ColumnNameContext)ctx.getChild(i + 3);					
			i += 4;
			
			AbstractType type = typeNameResolver(tnc);
			String columnName = cnc.getText().toUpperCase();
			
			processNthChild(vc, 0, NumberContext.class, n -> {
				Value v = type.fromString(columnName, n.getText());
				((SelectOperation)currentOperation).add(type, v.getName(), new ConstantExpression(v));
			});
			
			processNthChild(vc, 0, NullValueContext.class, n -> {
				Value v = AbstractType.wrapValue(columnName, null);
				((SelectOperation)currentOperation).add(type, v.getName(), new ConstantExpression(v));
			});
			
			processNthChild(vc, 0, StringContext.class, s -> {
				Value v = type.fromString(
					columnName, 
					rq(s.getChild(0).getText())
				);
				((SelectOperation)currentOperation).add(type, v.getName(), new ConstantExpression(v));
			});
			
			processNthChild(vc, 0, BoolContext.class, b -> {
				Value v = type.fromString(columnName, b.getText());
				((SelectOperation)currentOperation).add(type, v.getName(), new ConstantExpression(v));
			});
			
			processNthChild(vc, 0, ExecValueContext.class, e -> {
				((SelectOperation)currentOperation).add(
					type, 
					columnName, 
					buildScriptExpression((ExecValueContext)e)
				);
			});
			
			processNthChild(vc, 0, TimestampContext.class, t -> {
				Value v = type.fromString(
					columnName, 
					rq(t.getChild(1).getChild(0).getText())
				);
				((SelectOperation)currentOperation).add(
					type, 
					columnName, 
					new ConstantExpression(v)
				);
			});
		}
	}
	
	@Override
	public void enterCast(CastContext ctx) 
	{
		if (currentOperation == null || !(currentOperation instanceof SelectOperation))
		{
			throw new PDC2Exception("Недопустимое использование оператора: " + ctx.getText());
		}
		Integer i = 1;		
		while (i < ctx.getChildCount())
		{	
			ColumnNameContext cnc = 
				extractNthChild(ctx, i, ColumnNameContext.class).get();			 
			TypeNamesContext tnc = 
				extractNthChild(ctx, i + 2, TypeNamesContext.class).get();										
			i += 3;
			
			AbstractType type = typeNameResolver(tnc);
			((SelectOperation)currentOperation).cast(cnc.getText(), type);
		}
	}
		
	@Override
	public void enterRename(RenameContext ctx) 
	{
		if (currentOperation == null || !(currentOperation instanceof SelectOperation))
		{
			throw new PDC2Exception("Недопустимое использование оператора: " + ctx.getText());
		}
		Integer i = 1;		
		while (i < ctx.getChildCount())
		{	
			ColumnNameContext cncOld =
				extractNthChild(ctx, i, ColumnNameContext.class).get();					
			ColumnNameContext cncNew =
				extractNthChild(ctx, i + 2, ColumnNameContext.class).get();
			
			i += 3;
						
			((SelectOperation)currentOperation).rename(
					cncOld.getText().toUpperCase(), 
					cncNew.getText().toUpperCase()
			);
		}
	}
	
	@Override
	public void enterOrderBy(OrderByContext ctx) 
	{
		if (currentOperation == null || !(currentOperation instanceof SelectOperation))
		{
			throw new PDC2Exception("Недопустимое использование оператора: " + ctx.getText());
		}
		List<OrderByColumn> columnsList = new ArrayList<>();
		for (Integer i = 2; i < ctx.getChildCount(); i++)
		{
			OrderByColumnContext occ =
				extractNthChild(ctx, i, OrderByColumnContext.class).get();					
			String name = occ.getChild(0).getText().toUpperCase();
			Boolean desc = false;
			if (occ.getChildCount() > 1 && occ.getChild(1).getText().equals("убыв")) 
			{
				desc = true;
			}
			columnsList.add(new OrderByColumn(name, desc));
		}
		OrderByColumn[] oa = new OrderByColumn[columnsList.size()];
		columnsList.toArray(oa);
		((SelectOperation)currentOperation).orderBy(oa);
	}				

	@Override
	public void exitSelect(SelectContext ctx) 
	{
		currentOperation.execute();
		currentOperation = null;		
	}
	
	@Override
	public void enterInnerJoin(InnerJoinContext ctx) 
	{	
		if (targetDataSetName == null)
		{
			throw new PDC2Exception("Не завершена предыдущая операция для:" + ctx.getText());
		}
		DataSetNameContext dncLeft =
			extractNthChild(ctx, 2, DataSetNameContext.class).get();				
		DataSetNameContext dncRight =
			extractNthChild(ctx, 4, DataSetNameContext.class).get();			
		ColumnNameContext cncLeft =
			extractNthChild(ctx, 6, ColumnNameContext.class).get();			
		ColumnNameContext cncRight =
			extractNthChild(ctx, 8, ColumnNameContext.class).get();			
		AbstractDataSet leftDataSet = dataSetResolver(dncLeft.getText());
		AbstractDataSet rightDataSet = dataSetResolver(dncRight.getText());
		currentOperation = runtime.join(targetDataSetName)
			.innerJoin()
			.left(leftDataSet)
			.leftExpression(cncLeft.getText().toUpperCase())
			.right(rightDataSet)
			.rightExpression(cncRight.getText().toUpperCase());		
	}
			
	@Override
	public void exitInnerJoin(InnerJoinContext ctx) 
	{
		currentOperation.execute();
		currentOperation = null;
	}
		
	@Override
	public void enterLeftJoin(LeftJoinContext ctx) 
	{		
		if (targetDataSetName == null)
		{
			throw new PDC2Exception("Не завершена предыдущая операция для:" + ctx.getText());
		}
		DataSetNameContext dncLeft =
			extractNthChild(ctx, 2, DataSetNameContext.class).get();			
		DataSetNameContext dncRight =
			extractNthChild(ctx, 4, DataSetNameContext.class).get();			
		ColumnNameContext cncLeft =
			extractNthChild(ctx, 6, ColumnNameContext.class).get();				
		ColumnNameContext cncRight = 
			extractNthChild(ctx, 8, ColumnNameContext.class).get();			
		AbstractDataSet leftDataSet = dataSetResolver(dncLeft.getText());
		AbstractDataSet rightDataSet = dataSetResolver(dncRight.getText());
		currentOperation = runtime.join(targetDataSetName)			
			.left(leftDataSet)
			.leftExpression(cncLeft.getText().toUpperCase())
			.right(rightDataSet)
			.rightExpression(cncRight.getText().toUpperCase());
	}
	
	@Override
	public void exitLeftJoin(LeftJoinContext ctx) 
	{
		currentOperation.execute();
		currentOperation = null;
	}
		
	@Override
	public void enterGroupBy(GroupByContext ctx) 
	{
		if (targetDataSetName == null)
		{
			throw new PDC2Exception("Не завершена предыдущая операция для:" + ctx.getText());
		}
		DataSetNameContext dnc = (DataSetNameContext)ctx.getChild(1);
		List<String> groupColumns = new ArrayList<>();
		List<String> sumColumns = new ArrayList<>();
		List<String> maxColumns = new ArrayList<>();
		List<String> minColumns = new ArrayList<>();
		for (Integer i = 2; i < ctx.getChildCount(); i++)
		{
			if (ctx.getChild(i).getText().equals("по"))
			{
				GroupByColumnsContext gcc = 
					(GroupByColumnsContext)ctx.getChild(i + 1);
				for (Integer j = 0; j < gcc.getChildCount(); j++)
				{
					extractNthChild(gcc, j, ColumnNameContext.class).ifPresent( cnc -> {
						groupColumns.add(cnc.getText());
					});					
				}				
			}
			else if (ctx.getChild(i).getText().equals("сумма"))
			{
				GroupByColumnsContext gcc = 
						(GroupByColumnsContext)ctx.getChild(i + 1);
				for (Integer j = 0; j < gcc.getChildCount(); j++)
				{
					extractNthChild(gcc, j, ColumnNameContext.class).ifPresent( cnc -> {
						sumColumns.add(cnc.getText());
					});					
				}
			}
			else if (ctx.getChild(i).getText().equals("максимум"))
			{
				GroupByColumnsContext gcc = 
						(GroupByColumnsContext)ctx.getChild(i + 1);
				for (Integer j = 0; j < gcc.getChildCount(); j++)
				{
					extractNthChild(gcc, j, ColumnNameContext.class).ifPresent( cnc -> {
						maxColumns.add(cnc.getText());
					});
				}
			}
			else if (ctx.getChild(i).getText().equals("минимум"))
			{
				GroupByColumnsContext gcc = 
						(GroupByColumnsContext)ctx.getChild(i + 1);
				for (Integer j = 0; j < gcc.getChildCount(); j++)
				{
					extractNthChild(gcc, j, ColumnNameContext.class).ifPresent( cnc -> {
						minColumns.add(cnc.getText());
					});					
				}
			}
		}//for i
		
		String[] ca = new String[groupColumns.size()];
		groupColumns.toArray(ca);
		String[] sa = new String[sumColumns.size()];
		sumColumns.toArray(sa);
		String[] mxa = new String[maxColumns.size()];
		maxColumns.toArray(mxa);
		String[] mna = new String[minColumns.size()];
		minColumns.toArray(mna);
		
		AbstractDataSet dataSet = dataSetResolver(dnc.getText());
		currentOperation = runtime.groupBy(targetDataSetName)
			.from(dataSet)
				.columns(ca)
				.sum(sa)
				.max(mxa)
				.min(mna);
	}
		
	@Override
	public void exitGroupBy(GroupByContext ctx) 
	{
		currentOperation.execute();
		currentOperation = null;
	}

	@Override
	public void exitLet(LetContext ctx) 
	{
		targetDataSetName = null;
	}
				
	@Override
	public void enterWhere(WhereContext ctx) 
	{
		if (currentOperation == null || !(currentOperation instanceof SelectOperation))
		{
			throw new PDC2Exception("Недопустимое использование оператора: " + ctx.getText());
		}
		filtersStack.push(FilterBuilder.begin());
	}
		
	@Override
	public void exitWhere(WhereContext ctx) 
	{
		if (currentOperation == null || !(currentOperation instanceof SelectOperation))
		{
			throw new PDC2Exception("Недопустимое использование оператора: " + ctx.getText());
		}
		
		((SelectOperation)currentOperation).where(filtersStack.pop().end());
	}

	@Override
	public void enterCondition(ConditionContext ctx) 
	{
		if (filtersStack.isEmpty())
		{
			throw new PDC2Exception("Недопустимое использование оператора: " + ctx.getText());
		}
		
		ColumnNameContext cnc =
			extractNthChild(ctx, 0, ColumnNameContext.class).get();		
		String operation =
			extractNthChild(ctx, 1, TerminalNodeImpl.class).get().getText();
		AtomicReference<Expression> rightExpression = new AtomicReference<Expression>(null);		
		
		
		Optional<ColumnNameContext> cncRight = 
			extractNthChild(ctx, 2, ColumnNameContext.class);
						
		if (cncRight.isPresent())
		{				
			rightExpression.set(
				new ColumnNameExpression(cncRight.get().getText().toUpperCase())
			);			
		}
		else
		{
			ValueContext vc =
				extractNthChild(ctx, 2, ValueContext.class).get();
						
			processNthChild(vc, 0, NumberContext.class, nc -> {
				Comparable c = null;
				if (nc.getText().contains("."))
				{
					c = Double.parseDouble(nc.getText().replace("_", ""));
				}
				else
				{
					c = Long.parseLong(nc.getText().replace("_", ""));
				}
				rightExpression.set(new ConstantExpression(c));
			});
			
			processNthChild(vc, 0, NullValueContext.class, nvc -> {
				rightExpression.set(
					new ConstantExpression(AbstractType.wrapValue("", null))
				);
			});
			
			processNthChild(vc, 0, StringContext.class, sc -> {
				rightExpression.set(
					new ConstantExpression(rq(sc.getChild(0).getText()))
				);
			});
			
			processNthChild(vc, 0, BoolContext.class, bc -> {
				BooleanType bt = new BooleanType();				
				rightExpression.set(
					new ConstantExpression(bt.fromString("", bc.getText()))
				);
			});
			
			processNthChild(vc, 0, ExecValueContext.class, evc -> {
				rightExpression.set(
					buildScriptExpression((ExecValueContext)evc)
				);
			});
			
			processNthChild(vc, 0, TimestampContext.class, tc -> {
				TimestampType type = new TimestampType();
				Value v = type.fromString(
					"",
					rq(tc.getChild(1).getChild(0).getText())
				);
				rightExpression.set(new ConstantExpression(v));
			});
		}
		
		if (operation.equals("="))
		{
			filtersStack.peek().eq(
				new ColumnNameExpression(cnc.getText().toUpperCase()),
				rightExpression.get()
			);
		}
		else if (operation.equals(">"))
		{
			filtersStack.peek().gt(
				new ColumnNameExpression(cnc.getText().toUpperCase()),
				rightExpression.get()
			);
		}
		else if (operation.equals("<"))
		{
			filtersStack.peek().lt(
				new ColumnNameExpression(cnc.getText().toUpperCase()),
				rightExpression.get()
			);
		}
	}
	
	@Override
	public void enterAnd(AndContext ctx) 
	{
		if (filtersStack.isEmpty())
		{
			throw new PDC2Exception("Недопустимое использование оператора: " + ctx.getText());
		}
		filtersStack.peek().and();
	}
	
	@Override
	public void enterOr(OrContext ctx) 
	{
		if (filtersStack.isEmpty())
		{
			throw new PDC2Exception("Недопустимое использование оператора: " + ctx.getText());
		}
		filtersStack.peek().or();
	}
	
	@Override
	public void enterNot(NotContext ctx) 
	{
		if (filtersStack.isEmpty())
		{
			throw new PDC2Exception("Недопустимое использование оператора: " + ctx.getText());
		}
		filtersStack.peek().not();
	}
	
	@Override
	public void enterBlockOperation(BlockOperationContext ctx) 
	{
		if (filtersStack.isEmpty())
		{
			throw new PDC2Exception("Недопустимое использование оператора: " + ctx.getText());
		}
		FilterBuilder blockFilterBuilder = FilterBuilder.begin();
		filtersStack.push(blockFilterBuilder);
	}
		
	@Override
	public void exitBlockOperation(BlockOperationContext ctx) 
	{
		if (filtersStack.isEmpty())
		{
			throw new PDC2Exception("Недопустимое использование оператора: " + ctx.getText());
		}
		FilterBuilder blockFilterBuilder = filtersStack.pop();
		filtersStack.peek().block(blockFilterBuilder.end());
	}
	
	@Override
	public void enterUnion(UnionContext ctx) 
	{
		if (currentOperation == null || !(currentOperation instanceof SelectOperation))
		{
			throw new PDC2Exception("Недопустимое использование оператора: " + ctx.getText());
		}
		DataSetNameContext dnc =
			extractNthChild(ctx, 2, DataSetNameContext.class).get();
		((SelectOperation)currentOperation).union(
			dataSetResolver(dnc.getText()).flux()
		);
	}

	@Override
	public void enterShow(ShowContext ctx) 
	{		
		DataSetNameContext dataSetCtx =
			extractNthChild(ctx, 1, DataSetNameContext.class).get();				
		runtime.show(dataSetCtx.getText());
	}
		
	@Override
	public void enterRet(RetContext ctx) 
	{
		DataSetNameContext dnc =
			extractNthChild(ctx, 1, DataSetNameContext.class).get();
		runtime.ret(dnc.getText());
	}
	
	@Override
	public void enterExec(ExecContext ctx) 
	{		
		String code = ctx.getText().replaceAll("^\\{\\%", "").replaceAll("\\%\\}$", "");	
		runtime.exec(code);		
	}		

	@Override
	public void enterSave(SaveContext ctx) 
	{
		String dataSetName = ctx.getChild(1).getText();
		
		processNthChild(ctx, 3, StringContext.class, sc -> {
			String fileName = rq(sc.getChild(0).getText());
			runtime.save(dataSetName, fileName);
		});
		
		processNthChild(ctx, 3, DataSetNameContext.class, dnc -> {
			String consumerName = dnc.getText();
			runtime.saveWithConsumer(dataSetName, consumerName);
		});				
	}
		
	@Override
	public void enterUsing(UsingContext ctx) 
	{
		final HashMap<String, String> localParameters = new HashMap<>();
		parameters.forEach( (k, v) -> {
			localParameters.put(k, v);
		});
		String fileName =
			processNthChild(ctx, 1, StringContext.class, sc -> {
				return rq(sc.getChild(0).getText()); 
			});			
		for (Integer j = 0; j < ctx.getChildCount(); j++)
		{			
			processNthChild(ctx, j, SourceParamContext.class, spc -> {								
				String name = rq(spc.getChild(0).getChild(0).getText());
				String value = rq(spc.getChild(2).getChild(0).getText());
				localParameters.put(name, value);				
			});
		}
		runtime.eval(new File(fileName), localParameters);
	}
		
	@Override
	public void enterInvoke(InvokeContext ctx) 
	{
		if (targetDataSetName == null)
		{
			throw new PDC2Exception("Не завершена предыдущая операция для:" + ctx.getText());
		}
		final HashMap<String, String> localParameters = new HashMap<>();
		parameters.forEach( (k, v) -> {
			localParameters.put(k, v);
		});
		String fileName =
			processNthChild(ctx, 1, StringContext.class, sc -> {
				return rq(sc.getChild(0).getText()); 
			});
		for (Integer j = 0; j < ctx.getChildCount(); j++)
		{
			processNthChild(ctx, j, SourceParamContext.class, spc -> {								
				String name = rq(spc.getChild(0).getChild(0).getText());
				String value = rq(spc.getChild(2).getChild(0).getText());
				localParameters.put(name, value);				
			});
		}
		PDC2Runtime newRuntime = new PDC2Runtime();
		newRuntime.eval(new File(fileName), localParameters);
		if (newRuntime.getReturnedDataSet() != null)
		{
			runtime.getDataSets().put(targetDataSetName, newRuntime.getReturnedDataSet());
		}
		else
		{
			runtime.getDataSets().put(targetDataSetName, new ArrayDataSet());
		}
	}

	@Override
	public void enterSource(SourceContext ctx) 
	{
		if (targetDataSetName == null)
		{
			throw new PDC2Exception("Не завершена предыдущая операция для:" + ctx.getText());
		}
		
		HashMap<String, String> parameters = new HashMap<>();
		for (Integer j = 0; j < ctx.getChildCount(); j++)
		{
			processNthChild(ctx, j, SourceParamContext.class, spc -> {								
				String name = rq(spc.getChild(0).getChild(0).getText());
				String value = rq(spc.getChild(2).getChild(0).getText());
				parameters.put(name, value);				
			});
		}
		runtime.defineSource(targetDataSetName, parameters);
	}
	
	@Override
	public void enterConsumer(ConsumerContext ctx) 
	{
		if (targetDataSetName == null)
		{
			throw new PDC2Exception("Не завершена предыдущая операция для:" + ctx.getText());
		}
		
		HashMap<String, String> parameters = new HashMap<>();
		for (Integer j = 0; j < ctx.getChildCount(); j++)
		{
			processNthChild(ctx, j, SourceParamContext.class, spc -> {								
				String name = rq(spc.getChild(0).getChild(0).getText());
				String value = rq(spc.getChild(2).getChild(0).getText());
				parameters.put(name, value);				
			});
		}
		runtime.defineConsumer(targetDataSetName, parameters);
	}

	@Override
	public void enterSourceParam(SourceParamContext ctx) 
	{			
	}		

	public PDC2RuntimeListener(PDC2Runtime runtime, HashMap<String, String> parameters) 
	{
		super();
		this.runtime = runtime;
		this.parameters = parameters;
	}	
}
