package pdc2.core;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.glassfish.grizzly.http.server.ErrorPageGenerator;
import org.glassfish.grizzly.http.server.HttpHandler;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.grizzly.http.server.NetworkListener;
import org.glassfish.grizzly.http.server.Request;
import org.glassfish.grizzly.http.server.Response;
import org.glassfish.grizzly.http.server.StaticHttpHandler;

import pdc2.core.antlr.params.WebParams;

public class Main 
{
	public static byte[] readFromStream(InputStream is, Integer size) throws IOException
	{
		try (ByteArrayOutputStream baos = new ByteArrayOutputStream())
		{
			byte[] buf = new byte[8192];
			int len = is.read(buf, 0, buf.length);
			while (baos.size() < size) 
			{
				baos.write(buf, 0, len);
				len = is.read(buf, 0, buf.length);
			}
			baos.flush();
			return baos.toByteArray();
		}
	}
	
	public static void startWebConsole(Integer port) throws Throwable
	{
		HttpServer server = new HttpServer(); 
        NetworkListener nl = new NetworkListener(
            "http-listener", "0.0.0.0", port
        );
        server.addListener(nl);	            
        server.getServerConfiguration().addHttpHandler(
            new HttpHandler() {
				
				@Override
				public void service(Request req, Response res) throws Exception 
				{
					String path = req.getRequestURI();
					if (path.equals("/"))
					{
						path = "/index.html";
					}
					if (path.endsWith(".html"))
					{
						res.setContentType("text/html");
					}
					else if (path.endsWith(".js"))
					{								
						res.setContentType("text/javascript");
					}
					else if (path.endsWith(".css"))
					{
						res.setContentType("text/css");
					}
					else if (path.endsWith(".gif"))
					{
						res.setContentType("image/gif");
					}
					else
					{
						res.setContentType("application/json");
					}
					InputStream is = Main.class.getResourceAsStream("/www" + path);
					if (is == null)
					{
						res.sendError(404);
					}
					else
					{
						try
						{
							OutputStream os = res.getOutputStream();
							
							byte[] buf = new byte[4096];
							
							int len = is.read(buf, 0, buf.length);
							while (len > 0)
							{
								os.write(buf, 0, len);
								len = is.read(buf, 0, buf.length);
							}
							
							os.flush();
						}
						finally
						{
							is.close();
						}
					}
				}
			}, "/"
        );	            
        server.getServerConfiguration().addHttpHandler(
    		new HttpHandler() {						
				@Override
				public void service(Request req, Response res) throws Exception 
				{
					HashMap<String, String> parameters = new HashMap<>();
					Integer len = req.getContentLength();
					byte[] buf = Main.readFromStream(req.getInputStream(), len);
					String s = new String(buf, "UTF-8");
					if (s.startsWith("#"))
					{								
						String[] sa = s.split("\r?\n", 2);
						if (sa.length == 2)
						{
							s = sa[1];
							final WebParams webParams = new WebParams();
							webParams.process(sa[0].substring(1)).forEach( (n, v) -> {
								parameters.put(n, v);
							});									
						}
					}
					
					PDC2Runtime pdc2 = new PDC2Runtime();
					try
					{
						pdc2.eval(s, parameters);
						
						String resString = "";
						if (pdc2.getReturnedDataSet() != null)
						{
							resString = pdc2.getReturnedDataSet().toJson();
						}
						
						res.setCharacterEncoding("UTF-8");							
						res.getWriter().write(resString);
						res.getWriter().flush();
					}
					catch (Throwable t)
					{
						res.setCharacterEncoding("UTF-8");
						res.setErrorPageGenerator(new ErrorPageGenerator() 
						{									
							@Override
							public String generate(Request arg0, int arg1, String arg2, String arg3, Throwable e) 
							{
								return t.getMessage();
							}
						});
						res.sendError(500, t.getMessage());
						t.printStackTrace();
					}
				}
			}, 
    		"/exec"
		);
        server.getServerConfiguration().addHttpHandler(
    		new HttpHandler() {						
				@Override
				public void service(Request req, Response res) throws Exception 
				{
					String path = req.getRequestURI();
					path = path.replaceFirst("/load/", "");
					File f = new File(".", path);
					if (!f.exists() || !f.isFile())
					{
						res.sendError(404);									
					}
					try (FileInputStream fis = new FileInputStream(f))
					{
						byte[] buf = new byte[fis.available()];
						fis.read(buf, 0, fis.available());
						res.setContentType("text/pdc2");
						res.getOutputStream().write(buf);
						res.getOutputStream().flush();;
					}
				}
			}, 
    		"/load"
		);
        server.getServerConfiguration().addHttpHandler(
    		new HttpHandler() {						
				@Override
				public void service(Request req, Response res) throws Exception 
				{
					String path = req.getRequestURI();
					path = path.replaceFirst("/save/", "");
					File f = new File(".", path);
					Integer len = req.getContentLength();
					byte[] buf = Main.readFromStream(req.getInputStream(), len);
					
					try (FileOutputStream fos = new FileOutputStream(f))
					{
						fos.write(buf, 0, buf.length);
						fos.flush();
						res.sendAcknowledgement();
					}
				}
			}, 
    		"/save"
		);
        server.getServerConfiguration().addHttpHandler(
    		new HttpHandler() {						
				@Override
				public void service(Request req, Response res) throws Exception 
				{					
					File dir = new File("./demo");
					List<String> list = Stream.of(dir.listFiles())
						.filter( f -> f.getName().endsWith(".pdc2") && f.isFile())
						.map( f -> f.getName())
						.sorted( (a, b) -> {
							Integer i1 = Integer.parseInt(a.replace("demo", "").replace(".pdc2", ""));
							Integer i2 = Integer.parseInt(b.replace("demo", "").replace(".pdc2", ""));
							return i1.compareTo(i2);
						})
						.map( s -> "\"" + s + "\"")						
						.collect(Collectors.toList());
					
					String json = "";
					
					for (String s : list)
					{
						if (!json.equals(""))
						{
							json += ",";
						}
						json += s;
					}										
					
					json = "[" + json + "]";
					byte[] buf = json.getBytes("UTF-8");
					
					res.setContentType("applicaton/json; charset=utf-8");
					res.setContentLength(buf.length);
					OutputStream os = res.getOutputStream();
					os.write(buf);
					os.flush();
				}
			}, 
    		"/get-demo-files"
		);
        server.start();
        while (true) {
            Thread.sleep(1000L);
        }
	}
	
	public static void main(String[] args) throws Throwable 
	{		
		if (args.length <= 0)
		{
			System.out.println("Usage:");
			System.out.println("\tpdc web 8080");
			System.out.println("\tpdc run file.pdc2 [param=value]");
		}
		else
		{
			String mode = args[0];
			if (mode.equals("web"))
			{
				Integer port = Integer.parseInt(args[1]);
				startWebConsole(port);				
			}
			else if (mode.equals("run"))
			{
				HashMap<String, String> parameters = new HashMap<>();
				String fileName = args[1];
				Arrays.stream(args)
					.skip(2)
					.filter( ps -> ps.contains("="))
					.map( ps -> ps.split("="))
					.forEach( psa -> {											
						parameters.put(psa[0], psa[1]);
					});
				PDC2Runtime pdc2 = new PDC2Runtime();
				pdc2.eval(new File(fileName), parameters);
			}
			else
			{
				System.out.println("Unsupported mode: " + mode);
			}
		}
	}
}
