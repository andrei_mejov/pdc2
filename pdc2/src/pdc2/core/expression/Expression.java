package pdc2.core.expression;

import pdc2.core.dataset.Row;
import pdc2.core.dataset.Value;

public interface Expression 
{
	public Value eval(Row row);
}
