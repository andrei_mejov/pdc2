package pdc2.core.expression;

import pdc2.core.dataset.Row;
import pdc2.core.dataset.Value;
import pdc2.core.type.AbstractType;

public class EqExpression extends BooleanExpression 
{
	private Expression leftExpression;
	private Expression rightExpression;
			
	public EqExpression(Expression leftExpression, Expression rightExpression) 
	{
		super();
		this.leftExpression = leftExpression;
		this.rightExpression = rightExpression;
	}
	
	@Override
	protected Value internalEval(Row row) 
	{
		Value l = leftExpression.eval(row);
		Value r = rightExpression.eval(row);
		Boolean b;
		if (!l.getType().getClass().equals(r.getType().getClass()))
		{
			if (l.getValue() == null && r.getValue() == null)
			{
				b = true;
			}
			else
			{
				b = false;
			}
		}
		else
		{
			b = l.compareTo(r) == 0;
		}
		
		return AbstractType.wrapValue("", b);
	}	
}
