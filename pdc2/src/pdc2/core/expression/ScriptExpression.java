package pdc2.core.expression;

import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import pdc2.core.ScriptUtil;
import pdc2.core.dataset.Row;
import pdc2.core.dataset.Value;
import pdc2.core.type.AbstractType;

public class ScriptExpression implements Expression
{
	private String scriptText;
	
	private ScriptEngine engine; 
			
	public ScriptExpression(String scriptText) 
	{
		super();
		this.scriptText = scriptText;
		engine = new ScriptEngineManager()
			.getEngineByName("nashorn");		
	}

	@Override
	public Value eval(Row row) 
	{		
		try
		{
			engine.getBindings(ScriptContext.ENGINE_SCOPE)
				.put("$строка", row);
			engine.getBindings(ScriptContext.ENGINE_SCOPE)
				.put("$", new ScriptUtil());
			return AbstractType.wrapValue("", (Comparable)engine.eval(scriptText));				
		}
		catch (Throwable t)
		{
			throw new RuntimeException(t);
		}		
	}	
}
