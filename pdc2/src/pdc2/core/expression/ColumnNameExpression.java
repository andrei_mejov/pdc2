package pdc2.core.expression;

import pdc2.core.dataset.Row;
import pdc2.core.dataset.Value;

public class ColumnNameExpression implements Expression 
{
	private String columnName;
			
	public String getColumnName() 
	{
		return columnName;
	}

	public void setColumnName(String columnName) 
	{
		this.columnName = columnName;
	}

	public ColumnNameExpression(String columnName) 
	{
		super();
		this.columnName = columnName;
	}

	@Override
	public Value eval(Row row) 
	{
		return row.getByName(columnName); 
	}	
}
