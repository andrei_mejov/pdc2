package pdc2.core.expression;

import pdc2.core.dataset.Row;
import pdc2.core.dataset.Value;
import pdc2.core.type.AbstractType;

public class NotExpression extends BooleanExpression 
{
	private BooleanExpression expression;
	
	public NotExpression(BooleanExpression expression) 
	{
		super();
		this.expression = expression;
	}

	@Override
	protected Value internalEval(Row row) 
	{
		Value b = AbstractType.wrapValue("",
			!((Boolean)expression.eval(row).getValue())
		);
		return b;
	}		
}
