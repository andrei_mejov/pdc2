package pdc2.core.expression;

import pdc2.core.dataset.Row;
import pdc2.core.dataset.Value;
import pdc2.core.type.AbstractType;

public class ConstantExpression implements Expression 
{
	private Value value;
	
	public ConstantExpression(Comparable v) 
	{
		this(AbstractType.wrapValue("", v));		
	}

	public ConstantExpression(Value value) 
	{
		super();
		this.value = value;
	}

	@Override
	public Value eval(Row row) 
	{	
		return this.value;
	}	
}
