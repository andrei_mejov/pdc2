package pdc2.core.expression;

import pdc2.core.dataset.Row;
import pdc2.core.dataset.Value;
import pdc2.core.type.AbstractType;

public class OrExpression extends BooleanExpression 
{
	private BooleanExpression leftExpression;
	private BooleanExpression rightExpression;		
	
	public OrExpression(BooleanExpression leftExpression, BooleanExpression rightExpression) 
	{
		super();
		this.leftExpression = leftExpression;
		this.rightExpression = rightExpression;
	}

	@Override
	protected Value internalEval(Row row) 
	{	
		Value b = AbstractType.wrapValue(
			"",			
			(Boolean)leftExpression.eval(row).getValue() == true ||
			(Boolean)rightExpression.eval(row).getValue() == true
		);
		return b;
	}
}
