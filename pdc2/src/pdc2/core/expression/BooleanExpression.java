package pdc2.core.expression;

import pdc2.core.dataset.Row;
import pdc2.core.dataset.Value;
import pdc2.core.type.BooleanType;

public abstract class BooleanExpression implements Expression 
{
	protected abstract Value internalEval(Row row);	
	
	@Override
	public Value eval(Row row) 
	{
		Value v = internalEval(row);
		if (!(v.getType() instanceof BooleanType))
		{
			throw new RuntimeException("Illegal value for boolean expresion");
		}
		return v;
	}	
}
