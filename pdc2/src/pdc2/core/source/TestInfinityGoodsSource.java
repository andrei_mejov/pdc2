package pdc2.core.source;

import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicLong;

import pdc2.core.dataset.AbstractDataSet;
import pdc2.core.dataset.Row;
import pdc2.core.type.AbstractType;

public class TestInfinityGoodsSource implements PDC2Source
{	
	@Override
	public void init(HashMap<String, String> parameters) 
	{	
	}

	@Override
	public AbstractDataSet fetch() 
	{
		return new AbstractDataSet() {
			@Override
			public Iterator<Row> iterator() 
			{
				return new Iterator<Row>() 
				{
					private AtomicLong codeGenerator = new AtomicLong();
				
					@Override
					public Row next() 
					{
						long code = codeGenerator.incrementAndGet();
						Row row = new Row();
						row.getValues().add(AbstractType.wrapValue("КОД", code));
						row.getValues().add(AbstractType.wrapValue("НАИМЕНОВАНИЕ", "Товар" + code));
						return row;
					}
					
					@Override
					public boolean hasNext() 
					{
						return true;
					}
				};
			}
		};
	}
}
