package pdc2.core.source;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import pdc2.core.dataset.AbstractDataSet;
import pdc2.core.dataset.ColumnMetaData;
import pdc2.core.dataset.Row;
import pdc2.core.dataset.Value;
import pdc2.core.type.AbstractType;
import pdc2.core.type.BooleanType;
import pdc2.core.type.DoubleType;
import pdc2.core.type.LongType;
import pdc2.core.type.StringType;
import pdc2.core.type.TimestampType;

public class JDBCSource implements PDC2Source 
{
	public static final String DRIVER = "driver";
	public static final String URL = "url";
	public static final String USER = "user";
	public static final String PASSWORD = "password";
	public static final String REQUEST = "request";
	
	private HashMap<String, String> parameters;
	
	public void init(HashMap<String, String> parameters)
	{
		this.parameters = parameters;
	}
	
	@Override
	public AbstractDataSet fetch() 
	{
		try
		{
			if (parameters.containsKey(DRIVER))
			{
				DriverManager.registerDriver(
					(Driver)Class.forName(parameters.get(DRIVER)).newInstance()
				);
			}
			Connection connection = DriverManager.getConnection(
				parameters.get(URL), 
				parameters.get(USER),
				parameters.get(PASSWORD)
			);
			try
			{
				final Statement s = connection.createStatement();				
				final ResultSet r = s.executeQuery(parameters.get(REQUEST));
				
				final List<ColumnMetaData> columns = new ArrayList<>();
				
				for (Integer i = 0; i < r.getMetaData().getColumnCount(); i++)
				{
					String className = r.getMetaData().getColumnClassName(i + 1);
					Class clazz = Class.forName(className);
					String columnName = r.getMetaData().getColumnName(i + 1);
					
					AbstractType at = null;
					if (clazz.equals(String.class))
					{
						at = new StringType();
					}
					else if (clazz.equals(Integer.class) || clazz.equals(Long.class) || clazz.equals(BigDecimal.class))
					{
						at = new LongType();
					}
					else if (clazz.equals(Double.class))
					{
						at = new DoubleType();
					}
					else if (clazz.equals(Boolean.class))						
					{
						at = new BooleanType();
					}
					else if (Date.class.isAssignableFrom(clazz))
					{						
						at = new TimestampType();
					}					
					else
					{
						throw new RuntimeException("Unsupported type: " + clazz);
					}										
										
					ColumnMetaData cmd = new ColumnMetaData(columnName.toUpperCase(), at);
					columns.add(cmd);
				}//for
				
				return new AbstractDataSet() {					
					@Override
					public Iterator<Row> iterator() {
						return new Iterator<Row>() {							
							@Override
							public Row next() 
							{
								try
								{
									Row row = new Row();
									for (Integer i = 0; i < r.getMetaData().getColumnCount(); i++)
									{
										ColumnMetaData cmd = columns.get(i);
										AbstractType type = cmd.getType();
										Value v = new Value();
										v.setType(cmd.getType());
										v.setName(cmd.getName());
										if (type instanceof StringType)
										{
											v.setValue(r.getString(i + 1));
										}
										else if (type instanceof LongType)
										{
											v.setValue(r.getLong(i + 1));
										}
										else if (type instanceof DoubleType)
										{
											v.setValue(r.getDouble(i + 1));
										}
										else if (type instanceof BooleanType)
										{
											v.setValue(r.getBoolean(i + 1));
										}
										else if (type instanceof TimestampType)
										{							
											v.setValue(r.getTimestamp(i + 1));
										}
										else
										{
											throw new RuntimeException("Unsupported type: " + type);
										}
										
										row.getValues().add(v);
									}
									return row;
								}
								catch (SQLException e)
								{
									throw new RuntimeException(e);
								}
							}
							
							@Override
							public boolean hasNext() 
							{
								try
								{
									Boolean n = r.next();
									if (!n)
									{
										r.close();
										s.close();
									}
									return n;
								}
								catch (SQLException e)
								{
									throw new RuntimeException(e);
								}
							}
						};//Iterator<Row>
					}//iterator()
				};//AbstractDataSet				
			}
			finally
			{
				connection.close();
			}
		}
		catch (Throwable t)
		{
			throw new RuntimeException(t);
		}
	}	
}
