package pdc2.core.source;

import static org.junit.Assert.assertFalse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.lambdaworks.redis.RedisClient;
import com.lambdaworks.redis.RedisURI;
import com.lambdaworks.redis.api.StatefulRedisConnection;

import pdc2.core.dataset.AbstractDataSet;
import pdc2.core.dataset.ArrayDataSet;
import pdc2.core.dataset.Row;
import pdc2.core.type.AbstractType;

public class RedisHashSource implements PDC2Source 
{
	public static final String URL = "url";
	public static final String KEYS_PREFIX = "keysPrefix";
	public static final String REMOVE_AFTER = "removeAfter";
	
	private HashMap<String, String> parameters;
	
	@Override
	public void init(HashMap<String, String> parameters) 
	{	
		this.parameters = parameters;
	}

	@Override
	public AbstractDataSet fetch() 
	{
		final ArrayDataSet ds = new ArrayDataSet();
		RedisClient redisClient = RedisClient.create(
			RedisURI.create(parameters.get(URL))
		);
		try
		{
			StatefulRedisConnection<String, String> connection = redisClient.connect();
			try
			{		
				final List<String> orderList = new ArrayList<>();
				for (String s : connection.sync().keys(parameters.get(KEYS_PREFIX)))
				{
					final Row row = new Row();
					Map<String, String> map = connection.sync().hgetall(s);
					if (orderList.isEmpty()) 
					{
						map.keySet().forEach( n -> orderList.add(n));						
					}
					orderList.forEach( n -> {
						row.getValues().add(
							AbstractType.wrapValue(n.toUpperCase(), map.get(n))
						);
					});					
					ds.getRows().add(row);
					if (
							parameters.containsKey(REMOVE_AFTER) && 
							parameters.get(REMOVE_AFTER).equals("true")
						)
					{
						connection.sync().del(s);
					}
				}				
			}
			finally
			{
				connection.close();
			}
		}
		finally
		{
			redisClient.shutdown();
		}						
		return ds;
	}	
}
