package pdc2.core.source;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.poi.util.IOUtils;

public class CSVFromURLCache 
{
	private static ConcurrentHashMap<String, File> files = 
			new ConcurrentHashMap<>();
	
	public static InputStream fetch(String url, Long cacheValidInterval) throws IOException
	{
		File localFile = new File(url);
		if (localFile.exists())
		{
			return new FileInputStream(localFile);
		}
		else
		{
			File cachedFile = null;
			if (files.containsKey(url))
			{
				Long lastModify = files.get(url).lastModified();
				if (System.currentTimeMillis() - lastModify < cacheValidInterval * 1000L)
				{
					cachedFile = files.get(url);
				}
			}
			
			if (cachedFile != null)
			{
				return new FileInputStream(cachedFile);
			}
			else
			{
				cachedFile = File.createTempFile("csv", "tmp");
				try (FileOutputStream os = new FileOutputStream(cachedFile))
				{
					URL netUrl = new URL(url);
					try (InputStream is = netUrl.openStream())
					{
						IOUtils.copy(is, os);
					}
					os.flush();
				}
						
			    files.put(url, cachedFile);
				return new FileInputStream(cachedFile);
			}
		}
	}
}
