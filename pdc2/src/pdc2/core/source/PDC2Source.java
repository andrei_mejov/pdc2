package pdc2.core.source;

import java.util.HashMap;

import pdc2.core.dataset.AbstractDataSet;

public interface PDC2Source 
{
	public void init(HashMap<String, String> parameters);
	public AbstractDataSet fetch();
}
