package pdc2.core.source;

import java.sql.Date;
import java.util.HashMap;


import org.bson.Document;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import pdc2.core.dataset.AbstractDataSet;
import pdc2.core.dataset.ArrayDataSet;
import pdc2.core.dataset.Row;
import pdc2.core.type.AbstractType;

public class MongoSource implements PDC2Source 
{
	public static final String HOST = "host";
	public static final String PORT = "port";
	public static final String DATABASE = "database";
	public static final String COLLECTION = "collection";
	public static final String FILTER = "filter";
	
	private HashMap<String, String> parameters;	
	
	@Override
	public void init(HashMap<String, String> parameters) 
	{
		this.parameters = parameters;
	}

	@Override
	public AbstractDataSet fetch() 
	{		
		ArrayDataSet ds = new ArrayDataSet();
		MongoClient mongo = new MongoClient(
			parameters.get(HOST),
			Integer.parseInt(parameters.get(PORT))
		); 
		try
		{
			MongoDatabase db = mongo.getDatabase(parameters.get(DATABASE));
			MongoCollection<Document> collection = 
				db.getCollection(parameters.get(COLLECTION));
			FindIterable<Document> documnets = null;
			if (parameters.containsKey(FILTER))
			{
				try
				{
					Document cond = Document.parse(parameters.get(FILTER));									
					documnets = collection.find(cond);					
				}
				catch (Throwable t)
				{
					throw new RuntimeException(t);
				}
			}
			else
			{
				documnets = collection.find();
			}
			for (Document doc : documnets)
			{				
				final Row row = new Row();
				doc.forEach( (n, v) -> {
					if (v instanceof String || v instanceof Double ||
						v instanceof Integer || v instanceof Long || v instanceof Boolean ||
						v instanceof Date)
					{
						row.getValues().add(
							AbstractType.wrapValue(n.toUpperCase(), (Comparable)v)
						);
					}
				});
				ds.getRows().add(row);
				
			}
			return ds;
		}
		finally
		{
			mongo.close();
		}			
	}	
}
