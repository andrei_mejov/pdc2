package pdc2.core.source;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import pdc2.core.dataset.AbstractDataSet;
import pdc2.core.dataset.ArrayDataSet;
import pdc2.core.dataset.Row;
import pdc2.core.dataset.Value;
import pdc2.core.type.AbstractType;

public class XLSXSource implements PDC2Source
{
	public static final String FILE = "file";
	
	private HashMap<String, String> parameters = new HashMap<>();

	@Override
	public void init(HashMap<String, String> parameters) 
	{
		this.parameters = parameters;
	}

	@Override
	public AbstractDataSet fetch() 
	{
		try
		{
			ArrayDataSet dataSet = new ArrayDataSet();			
			XSSFWorkbook wb = new XSSFWorkbook(new File(parameters.get(FILE)));
			List<String> columnNames = new ArrayList<>();
			try
			{
				Sheet sheet = wb.getSheetAt(0);
				if (sheet.getLastRowNum() > 0)
				{
					Integer i = 0;
					while (true)
					{
						Cell cell = sheet.getRow(0).getCell(i);
						if (cell == null)
						{
							break;
						}
						String columnName = cell.getStringCellValue();
						if (columnName == null || columnName.trim().equals(""))
						{
							break;
						}						
						columnNames.add(columnName.toUpperCase());						
						i++;
					}
					i = 1;
					while (i <= sheet.getLastRowNum())
					{
						Row row = new Row();
						for (Integer j = 0; j < columnNames.size(); j++)
						{
							Cell cell = sheet.getRow(i).getCell(j);
							Value v;
							if (cell != null)
							{
								v = AbstractType.wrapValue(columnNames.get(j), cell.toString());
							}
							else
							{
								v = AbstractType.wrapValue(columnNames.get(j), null);								
							}							
							row.getValues().add(v);
						}//for
						dataSet.getRows().add(row);
						i++;
					}//while
				}
			}
			finally
			{
				wb.close();
			}
			return dataSet;
		}
		catch (Throwable t)
		{
			throw new RuntimeException(t);
		}	
	}		
}
