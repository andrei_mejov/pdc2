package pdc2.core.source;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicLong;

import pdc2.core.dataset.AbstractDataSet;
import pdc2.core.dataset.Row;
import pdc2.core.type.AbstractType;

public class TestInfinitySalesSource implements PDC2Source
{
	@Override
	public void init(HashMap<String, String> parameters) 
	{	
	}

	@Override
	public AbstractDataSet fetch() 
	{
		return new AbstractDataSet() {
			@Override
			public Iterator<Row> iterator() 
			{
				return new Iterator<Row>() 
				{
					private AtomicLong codeGenerator = new AtomicLong(0L);
					private AtomicLong departmentGenerator = new AtomicLong(1L);
					private Calendar calendar = Calendar.getInstance();
					private Date startDate = new Date();
				
					@Override
					public synchronized Row next() 
					{
						long department = departmentGenerator.get();
						long code = codeGenerator.incrementAndGet();
						long price = 10000L + code;
						long amount = code % 2 == 0 ? 2L : 1L;
						double sum = price * amount / 100.0;
						Date date = calendar.getTime();
						Row row = new Row();
						row.getValues().add(AbstractType.wrapValue("КОД", code));
						row.getValues().add(AbstractType.wrapValue("ПОДРАЗДЕЛЕНИЕ", "Магазин" + department));
						row.getValues().add(AbstractType.wrapValue("ДАТА", date));
						row.getValues().add(AbstractType.wrapValue("ЦЕННА", price));
						row.getValues().add(AbstractType.wrapValue("КОЛИЧЕСТВО", amount));
						row.getValues().add(AbstractType.wrapValue("СУММА", sum));
						row.getValues().add(AbstractType.wrapValue("БЕЗНАЛ", code % 2 == 0 ? true : false));
						
						if (code >= 10L)
						{
							departmentGenerator.incrementAndGet();
							codeGenerator.set(0);
						}
						
						if (departmentGenerator.get() > 2) 
						{
							departmentGenerator.set(1);
							codeGenerator.set(0);
							calendar.add(Calendar.DAY_OF_YEAR, -1);
						}
						
						return row;
					}
					
					@Override
					public boolean hasNext() 
					{
						return true;
					}
				};
			}
		};
	}
}
