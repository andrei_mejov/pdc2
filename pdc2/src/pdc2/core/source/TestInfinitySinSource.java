package pdc2.core.source;

import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicLong;

import pdc2.core.dataset.AbstractDataSet;
import pdc2.core.dataset.Row;
import pdc2.core.type.AbstractType;

public class TestInfinitySinSource implements PDC2Source 
{
	@Override
	public void init(HashMap<String, String> parameters) 
	{	
	}

	@Override
	public AbstractDataSet fetch() 
	{	
		return new AbstractDataSet() {
			@Override
			public Iterator<Row> iterator() 
			{
				return new Iterator<Row>() 
				{					
					private AtomicLong x = new AtomicLong(0L);
				
					@Override
					public Row next() 
					{
						double xd = x.incrementAndGet() * Math.PI/100;
						Row row = new Row();
						row.getValues().add(AbstractType.wrapValue("X", xd));
						row.getValues().add(AbstractType.wrapValue("SIN", Math.sin(xd)));
						return row;
					}
					
					@Override
					public boolean hasNext() 
					{
						return true;
					}
				};
			}
		};
	}
	
}
