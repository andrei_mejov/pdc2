package pdc2.core.source;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import pdc2.core.dataset.AbstractDataSet;
import pdc2.core.dataset.Row;
import pdc2.core.type.AbstractType;

public class CSVFromURLSource implements PDC2Source 
{
	//https://data.gov.ru/opendata/7703771271-ekp/data-20150914T1545-structure-20150914T1545.csv?encoding=UTF-8
	
	static {
		TrustManager[] trustAllCerts = new TrustManager[]{
		    new X509TrustManager() {
		        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
		            return null;
		        }
		        public void checkClientTrusted(
		            java.security.cert.X509Certificate[] certs, String authType) {
		        }
		        public void checkServerTrusted(
		            java.security.cert.X509Certificate[] certs, String authType) {
		        }
		    }
		};	
		
		try
		{
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		}
		catch (Throwable e) {e.printStackTrace();}
	}
	
	public static final String URL = "url";
	public static final String CHARSET = "charset";
	public static final String QUOUTED_VALUE = "quotedValue";
	public static final String DELIMITER = "delimiter";
	public static final String CACHE_VALID_INTERVAL = "cacheValidInterval";
	
	private HashMap<String, String> parameters;

	@Override
	public void init(HashMap<String, String> parameters) 
	{
		this.parameters = parameters;
	}

	@Override
	public AbstractDataSet fetch() 
	{
		final List<Row> rows = new ArrayList<>();
		try		
		{
			Long cacheValidInterval = Long.parseLong(parameters.getOrDefault(CACHE_VALID_INTERVAL, "3600"));
			try (BufferedReader reader = new BufferedReader(
				new InputStreamReader(CSVFromURLCache.fetch(parameters.get(URL), cacheValidInterval), parameters.get(CHARSET))
			))
			{				
				List<String> columnNames = null;
				String line = reader.readLine();
				Boolean isFirst = true;
				final Boolean isQuoted = parameters.containsKey(QUOUTED_VALUE) && 
						parameters.get(QUOUTED_VALUE).equals("true"); 
				while (line != null)
				{
					String[] sa = null;
					if (isQuoted)
					{
						sa = line.split(parameters.get(DELIMITER) + "(?=([^\\\"]*\\\"[^\\\"]*\\\")*[^\\\"]*$)");
					}
					else
					{
						sa = line.split(parameters.get(DELIMITER));
					}										
					
					if (isFirst)
					{
						columnNames = Stream.of(sa)
							.map(s -> s.trim())
							.map( s -> {
								if (isQuoted) {
									return s.replaceAll("^\"", "").replaceAll("\"$", "");
								} else {
									return s;
								}
							}).map( s -> s.toUpperCase())
							.collect(Collectors.toList());
						isFirst = false;
					} 
					else 
					{
						List<String> list = Stream.of(sa)
							.map(s -> s.trim())
							.map( s -> {
								if (isQuoted) {
									return s.replaceAll("^\"", "").replaceAll("\"$", "");
								} else {
									return s;
								}
							})
							.collect(Collectors.toList());						
						if (list.size() == columnNames.size())
						{/*Кривой CSV на открытых данных я видел*/
							Row row = new Row();
							for (Integer i = 0; i < list.size(); i++) 
							{
								String value = list.get(i);
								String name = columnNames.get(i);
								row.getValues().add(AbstractType.wrapValue(name, value));
							}						
							rows.add(row);
						}
					}
					
					line = reader.readLine();
				}
			}			
			return new AbstractDataSet() 
			{				
				@Override
				public Iterator<Row> iterator() 
				{					
					return rows.iterator();
				}
			};			
		}
		catch (Throwable t)
		{
			throw new RuntimeException(t);
		}
	}	
}
