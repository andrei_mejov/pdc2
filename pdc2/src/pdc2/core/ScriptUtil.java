package pdc2.core;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import pdc2.core.dataset.Row;

public class ScriptUtil 
{
	public Date parseDate(String pattern, String s) throws ParseException
	{
		SimpleDateFormat sdf = new SimpleDateFormat(pattern, Locale.US);
		return sdf.parse(s);
	}
	
	public Date разобратьДату(String pattern, String s) throws ParseException
	{
		return parseDate(pattern, s);
	}
	
	public String format(String pattern, Comparable o)
	{
		return String.format(pattern, o);
	}
	
	public String формат(String pattern, Comparable o)
	{
		return format(pattern, o);
	}
	
	public Comparable extract(Row row, String name)
	{
		return row.getByName(name).getValue();
	}
	
	public Comparable извлечь(Row row, String name)
	{
		return extract(row, name);
	}
}
