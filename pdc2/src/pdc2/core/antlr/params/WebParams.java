package pdc2.core.antlr.params;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;

import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;
import org.antlr.v4.runtime.misc.ParseCancellationException;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import pdc2.core.PDC2RuntimeListener;
import pdc2.core.antlr.params.ParamsParser.ParamContext;

public class WebParams
{
	public String rq(String s)
	{
		return s.replaceAll("^[\"']", "").replaceAll("[\"']$", "");
	}
	
	public HashMap<String, String> process(String s) throws IOException
	{
		final HashMap<String, String> params = new HashMap<>();
		
		ParamsLexer lexer = new ParamsLexer(
				CharStreams.fromStream(new ByteArrayInputStream(s.getBytes("UTF-8")), 
						Charset.forName("UTF-8")
				)
			);		
		ParamsParser parser = new ParamsParser(new CommonTokenStream(lexer));
			
		parser.addErrorListener(new BaseErrorListener() {
			@Override
			public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line,
					int charPositionInLine, String msg, RecognitionException e) {
				 throw new ParseCancellationException(
					 "" + line + ":" + charPositionInLine + " " + msg
				 );
			}				
		});
		
		ParamsBaseListener listener = new ParamsBaseListener() {
			@Override
			public void enterParam(ParamContext ctx) 
			{
				String name = ctx.getChild(0).getText();
				String value = ctx.getChild(2).getChild(0).getText();
				params.put(name, rq(value));
			}
			
		};
			
		ParseTreeWalker.DEFAULT.walk(listener, parser.r());
		
		return params;
	}
}
