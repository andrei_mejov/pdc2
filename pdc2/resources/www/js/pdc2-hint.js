// CodeMirror, copyright (c) by Marijn Haverbeke and others
// Distributed under an MIT license: http://codemirror.net/LICENSE

(function(mod) {
    mod(CodeMirror);
})(function(CodeMirror) {
  "use strict";
  
  var Pos = CodeMirror.Pos, cmpPos = CodeMirror.cmpPos;

  function isArray(val) { return Object.prototype.toString.call(val) == "[object Array]" }

  function getKeywords(editor) {
    var mode = editor.doc.modeOption;
    var ka = getPDC2Keywords();
    var kw = [];
    for (var i in ka)
    {
    	kw.push(i);
    }
    kw.sort();
    return kw;
  }

  function getIdentifierQuote(editor) {
    var mode = editor.doc.modeOption;
    if (mode === "pdc2") mode = "text/pdc2";
    return CodeMirror.resolveMode(mode).identifierQuote || "`";
  }

  function getText(item) {
    return typeof item == "string" ? item : item.text;
  }

  function match(string, word) {
    var len = string.length;
    var sub = getText(word).substr(0, len);
    return string.toUpperCase() === sub.toUpperCase();
  }

  function addMatches(result, search, wordlist, formatter) {
	  for (var i = 0; i < wordlist.length; i++) {
		  if (match(search, wordlist[i])) result.push(formatter(wordlist[i]));
	  }
  }

  CodeMirror.registerHelper("hint", "pdc2", function(editor, options) {
	var keywords = getKeywords(editor);
    var cur = editor.getCursor();
    var result = [];
    var token = editor.getTokenAt(cur), start, end, search;
    if (token.end > cur.ch) {
      token.end = cur.ch;
      token.string = token.string.slice(0, cur.ch - token.start);
    }

    if (token.string != null && token.string.trim() != "") {
      search = token.string;
      start = token.start;
      end = token.end;
    } else {
      start = end = cur.ch;
      search = "";
    }
    
    addMatches(
		result, 
		search, 
		keywords, 
		function(w) {return {text: w.toLowerCase(), className: "CodeMirror-hint-keyword"};}
	);
    return {list: result, from: Pos(cur.line, start), to: Pos(cur.line, end)};
  });
});
