grammar PDC2;

@lexer::members {
        boolean isStringValue = false;
}

r: expr*;

expr: (let | exec | show | save | using | ret | save) TERM; 

ret: 'вернуть' dataSetName;

using: 'использовать' string ('{' sourceParam+ '}')?;

invoke: 'выполнить' string ('{' sourceParam+ '}')?;

save: 'сохранить' dataSetName 'в' (string | dataSetName);

show: 'показать' dataSetName;

exec: CODE;

let: dataSetName EQ (pipedOperation | source | consumer | invoke);
pipedOperation: (select | innerJoin | leftJoin | groupBy) ('|' (select | innerJoin | leftJoin | groupBy))*;

sourceParam: string ':' string;
source: 'источник' '{' sourceParam+ '}';

consumer: 'потребитель' '{' sourceParam+ '}';

select: 'выбрать' 'из' (dataSetName | string) 
	(project | first | where | cast | rename | add | orderBy | union)*;
where: 'где' operation;
operation: condition | blockOperation | unaryOperation | binaryOperation;
blockOperation: '(' operation ')';
binaryOperation: (blockOperation | condition) (and | or) (blockOperation | condition);
unaryOperation: not (operation | blockOperation); 
condition: columnName (EQ | LT | GT) (value | columnName);
and: 'и';
or: 'или';
not: 'не';
orderBy: 'упорядочить' 'по' orderByColumn+;
orderByColumn: columnName 'убыв'?;
rename: 'переименовать' (columnName 'в' columnName)+;
typeNames: 'целое' | 'вещественное' | 'булево' | 'дата' | 'строка';
add: 'добавить' (typeNames value 'как' columnName)+;
cast: 'выразить' (columnName 'как' typeNames)+;
union: 'объединить' 'с' dataSetName;
project: 'столбцы' columnName+;
first: 'первые' number;

innerJoin: 'внутреннее' 'соединение' dataSetName 'и' dataSetName
	'по' columnName EQ columnName;
	
leftJoin: 'левое' 'соединение' dataSetName 'и' dataSetName
	'по' columnName EQ columnName;
	
groupBy: 'группировать' dataSetName 'по' groupByColumns
	( 
		( 'сумма' groupByColumns ) |
		( 'максимум' groupByColumns ) |
		( 'минимум' groupByColumns ) 
	)+;
groupByColumns: columnName+;

dataSetName: ID;
columnName: ID;

execValue: CODE;
valuesList: value (',' value)*;
value: string | number | bool | timestamp | execValue | nullValue;
nullValue: 'null';
timestamp: 'дата(' string ')';
number: NUM;
bool: 'истина' | 'ложь';
string: stringValue;
stringValue: LITERAL;

TERM: [;];
EQ : '=';
LT : '<';
GT : '>';
LITERAL: ('\'' {isStringValue = true;} ~('\'')* {isStringValue = false;} '\'') | ('"' {isStringValue = true;} ~('"')* {isStringValue = false;} '"');
NUM: [-]?[0-9._]+;
ID: [A-Za-zА-Яа-я0-9.*_]+;
COMMENT: '/*' .*? '*/' {if (!isStringValue) skip();};
CODE: '{%' .*? '%}';
WS : [ \t\r\n]+ {if (!isStringValue) skip();};