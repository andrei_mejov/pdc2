// Generated from PDC2.g4 by ANTLR 4.7.1
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link PDC2Parser}.
 */
public interface PDC2Listener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link PDC2Parser#r}.
	 * @param ctx the parse tree
	 */
	void enterR(PDC2Parser.RContext ctx);
	/**
	 * Exit a parse tree produced by {@link PDC2Parser#r}.
	 * @param ctx the parse tree
	 */
	void exitR(PDC2Parser.RContext ctx);
	/**
	 * Enter a parse tree produced by {@link PDC2Parser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr(PDC2Parser.ExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link PDC2Parser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr(PDC2Parser.ExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link PDC2Parser#ret}.
	 * @param ctx the parse tree
	 */
	void enterRet(PDC2Parser.RetContext ctx);
	/**
	 * Exit a parse tree produced by {@link PDC2Parser#ret}.
	 * @param ctx the parse tree
	 */
	void exitRet(PDC2Parser.RetContext ctx);
	/**
	 * Enter a parse tree produced by {@link PDC2Parser#using}.
	 * @param ctx the parse tree
	 */
	void enterUsing(PDC2Parser.UsingContext ctx);
	/**
	 * Exit a parse tree produced by {@link PDC2Parser#using}.
	 * @param ctx the parse tree
	 */
	void exitUsing(PDC2Parser.UsingContext ctx);
	/**
	 * Enter a parse tree produced by {@link PDC2Parser#invoke}.
	 * @param ctx the parse tree
	 */
	void enterInvoke(PDC2Parser.InvokeContext ctx);
	/**
	 * Exit a parse tree produced by {@link PDC2Parser#invoke}.
	 * @param ctx the parse tree
	 */
	void exitInvoke(PDC2Parser.InvokeContext ctx);
	/**
	 * Enter a parse tree produced by {@link PDC2Parser#save}.
	 * @param ctx the parse tree
	 */
	void enterSave(PDC2Parser.SaveContext ctx);
	/**
	 * Exit a parse tree produced by {@link PDC2Parser#save}.
	 * @param ctx the parse tree
	 */
	void exitSave(PDC2Parser.SaveContext ctx);
	/**
	 * Enter a parse tree produced by {@link PDC2Parser#show}.
	 * @param ctx the parse tree
	 */
	void enterShow(PDC2Parser.ShowContext ctx);
	/**
	 * Exit a parse tree produced by {@link PDC2Parser#show}.
	 * @param ctx the parse tree
	 */
	void exitShow(PDC2Parser.ShowContext ctx);
	/**
	 * Enter a parse tree produced by {@link PDC2Parser#exec}.
	 * @param ctx the parse tree
	 */
	void enterExec(PDC2Parser.ExecContext ctx);
	/**
	 * Exit a parse tree produced by {@link PDC2Parser#exec}.
	 * @param ctx the parse tree
	 */
	void exitExec(PDC2Parser.ExecContext ctx);
	/**
	 * Enter a parse tree produced by {@link PDC2Parser#let}.
	 * @param ctx the parse tree
	 */
	void enterLet(PDC2Parser.LetContext ctx);
	/**
	 * Exit a parse tree produced by {@link PDC2Parser#let}.
	 * @param ctx the parse tree
	 */
	void exitLet(PDC2Parser.LetContext ctx);
	/**
	 * Enter a parse tree produced by {@link PDC2Parser#pipedOperation}.
	 * @param ctx the parse tree
	 */
	void enterPipedOperation(PDC2Parser.PipedOperationContext ctx);
	/**
	 * Exit a parse tree produced by {@link PDC2Parser#pipedOperation}.
	 * @param ctx the parse tree
	 */
	void exitPipedOperation(PDC2Parser.PipedOperationContext ctx);
	/**
	 * Enter a parse tree produced by {@link PDC2Parser#sourceParam}.
	 * @param ctx the parse tree
	 */
	void enterSourceParam(PDC2Parser.SourceParamContext ctx);
	/**
	 * Exit a parse tree produced by {@link PDC2Parser#sourceParam}.
	 * @param ctx the parse tree
	 */
	void exitSourceParam(PDC2Parser.SourceParamContext ctx);
	/**
	 * Enter a parse tree produced by {@link PDC2Parser#source}.
	 * @param ctx the parse tree
	 */
	void enterSource(PDC2Parser.SourceContext ctx);
	/**
	 * Exit a parse tree produced by {@link PDC2Parser#source}.
	 * @param ctx the parse tree
	 */
	void exitSource(PDC2Parser.SourceContext ctx);
	/**
	 * Enter a parse tree produced by {@link PDC2Parser#consumer}.
	 * @param ctx the parse tree
	 */
	void enterConsumer(PDC2Parser.ConsumerContext ctx);
	/**
	 * Exit a parse tree produced by {@link PDC2Parser#consumer}.
	 * @param ctx the parse tree
	 */
	void exitConsumer(PDC2Parser.ConsumerContext ctx);
	/**
	 * Enter a parse tree produced by {@link PDC2Parser#select}.
	 * @param ctx the parse tree
	 */
	void enterSelect(PDC2Parser.SelectContext ctx);
	/**
	 * Exit a parse tree produced by {@link PDC2Parser#select}.
	 * @param ctx the parse tree
	 */
	void exitSelect(PDC2Parser.SelectContext ctx);
	/**
	 * Enter a parse tree produced by {@link PDC2Parser#where}.
	 * @param ctx the parse tree
	 */
	void enterWhere(PDC2Parser.WhereContext ctx);
	/**
	 * Exit a parse tree produced by {@link PDC2Parser#where}.
	 * @param ctx the parse tree
	 */
	void exitWhere(PDC2Parser.WhereContext ctx);
	/**
	 * Enter a parse tree produced by {@link PDC2Parser#operation}.
	 * @param ctx the parse tree
	 */
	void enterOperation(PDC2Parser.OperationContext ctx);
	/**
	 * Exit a parse tree produced by {@link PDC2Parser#operation}.
	 * @param ctx the parse tree
	 */
	void exitOperation(PDC2Parser.OperationContext ctx);
	/**
	 * Enter a parse tree produced by {@link PDC2Parser#blockOperation}.
	 * @param ctx the parse tree
	 */
	void enterBlockOperation(PDC2Parser.BlockOperationContext ctx);
	/**
	 * Exit a parse tree produced by {@link PDC2Parser#blockOperation}.
	 * @param ctx the parse tree
	 */
	void exitBlockOperation(PDC2Parser.BlockOperationContext ctx);
	/**
	 * Enter a parse tree produced by {@link PDC2Parser#binaryOperation}.
	 * @param ctx the parse tree
	 */
	void enterBinaryOperation(PDC2Parser.BinaryOperationContext ctx);
	/**
	 * Exit a parse tree produced by {@link PDC2Parser#binaryOperation}.
	 * @param ctx the parse tree
	 */
	void exitBinaryOperation(PDC2Parser.BinaryOperationContext ctx);
	/**
	 * Enter a parse tree produced by {@link PDC2Parser#unaryOperation}.
	 * @param ctx the parse tree
	 */
	void enterUnaryOperation(PDC2Parser.UnaryOperationContext ctx);
	/**
	 * Exit a parse tree produced by {@link PDC2Parser#unaryOperation}.
	 * @param ctx the parse tree
	 */
	void exitUnaryOperation(PDC2Parser.UnaryOperationContext ctx);
	/**
	 * Enter a parse tree produced by {@link PDC2Parser#condition}.
	 * @param ctx the parse tree
	 */
	void enterCondition(PDC2Parser.ConditionContext ctx);
	/**
	 * Exit a parse tree produced by {@link PDC2Parser#condition}.
	 * @param ctx the parse tree
	 */
	void exitCondition(PDC2Parser.ConditionContext ctx);
	/**
	 * Enter a parse tree produced by {@link PDC2Parser#and}.
	 * @param ctx the parse tree
	 */
	void enterAnd(PDC2Parser.AndContext ctx);
	/**
	 * Exit a parse tree produced by {@link PDC2Parser#and}.
	 * @param ctx the parse tree
	 */
	void exitAnd(PDC2Parser.AndContext ctx);
	/**
	 * Enter a parse tree produced by {@link PDC2Parser#or}.
	 * @param ctx the parse tree
	 */
	void enterOr(PDC2Parser.OrContext ctx);
	/**
	 * Exit a parse tree produced by {@link PDC2Parser#or}.
	 * @param ctx the parse tree
	 */
	void exitOr(PDC2Parser.OrContext ctx);
	/**
	 * Enter a parse tree produced by {@link PDC2Parser#not}.
	 * @param ctx the parse tree
	 */
	void enterNot(PDC2Parser.NotContext ctx);
	/**
	 * Exit a parse tree produced by {@link PDC2Parser#not}.
	 * @param ctx the parse tree
	 */
	void exitNot(PDC2Parser.NotContext ctx);
	/**
	 * Enter a parse tree produced by {@link PDC2Parser#orderBy}.
	 * @param ctx the parse tree
	 */
	void enterOrderBy(PDC2Parser.OrderByContext ctx);
	/**
	 * Exit a parse tree produced by {@link PDC2Parser#orderBy}.
	 * @param ctx the parse tree
	 */
	void exitOrderBy(PDC2Parser.OrderByContext ctx);
	/**
	 * Enter a parse tree produced by {@link PDC2Parser#orderByColumn}.
	 * @param ctx the parse tree
	 */
	void enterOrderByColumn(PDC2Parser.OrderByColumnContext ctx);
	/**
	 * Exit a parse tree produced by {@link PDC2Parser#orderByColumn}.
	 * @param ctx the parse tree
	 */
	void exitOrderByColumn(PDC2Parser.OrderByColumnContext ctx);
	/**
	 * Enter a parse tree produced by {@link PDC2Parser#rename}.
	 * @param ctx the parse tree
	 */
	void enterRename(PDC2Parser.RenameContext ctx);
	/**
	 * Exit a parse tree produced by {@link PDC2Parser#rename}.
	 * @param ctx the parse tree
	 */
	void exitRename(PDC2Parser.RenameContext ctx);
	/**
	 * Enter a parse tree produced by {@link PDC2Parser#typeNames}.
	 * @param ctx the parse tree
	 */
	void enterTypeNames(PDC2Parser.TypeNamesContext ctx);
	/**
	 * Exit a parse tree produced by {@link PDC2Parser#typeNames}.
	 * @param ctx the parse tree
	 */
	void exitTypeNames(PDC2Parser.TypeNamesContext ctx);
	/**
	 * Enter a parse tree produced by {@link PDC2Parser#add}.
	 * @param ctx the parse tree
	 */
	void enterAdd(PDC2Parser.AddContext ctx);
	/**
	 * Exit a parse tree produced by {@link PDC2Parser#add}.
	 * @param ctx the parse tree
	 */
	void exitAdd(PDC2Parser.AddContext ctx);
	/**
	 * Enter a parse tree produced by {@link PDC2Parser#cast}.
	 * @param ctx the parse tree
	 */
	void enterCast(PDC2Parser.CastContext ctx);
	/**
	 * Exit a parse tree produced by {@link PDC2Parser#cast}.
	 * @param ctx the parse tree
	 */
	void exitCast(PDC2Parser.CastContext ctx);
	/**
	 * Enter a parse tree produced by {@link PDC2Parser#union}.
	 * @param ctx the parse tree
	 */
	void enterUnion(PDC2Parser.UnionContext ctx);
	/**
	 * Exit a parse tree produced by {@link PDC2Parser#union}.
	 * @param ctx the parse tree
	 */
	void exitUnion(PDC2Parser.UnionContext ctx);
	/**
	 * Enter a parse tree produced by {@link PDC2Parser#project}.
	 * @param ctx the parse tree
	 */
	void enterProject(PDC2Parser.ProjectContext ctx);
	/**
	 * Exit a parse tree produced by {@link PDC2Parser#project}.
	 * @param ctx the parse tree
	 */
	void exitProject(PDC2Parser.ProjectContext ctx);
	/**
	 * Enter a parse tree produced by {@link PDC2Parser#first}.
	 * @param ctx the parse tree
	 */
	void enterFirst(PDC2Parser.FirstContext ctx);
	/**
	 * Exit a parse tree produced by {@link PDC2Parser#first}.
	 * @param ctx the parse tree
	 */
	void exitFirst(PDC2Parser.FirstContext ctx);
	/**
	 * Enter a parse tree produced by {@link PDC2Parser#innerJoin}.
	 * @param ctx the parse tree
	 */
	void enterInnerJoin(PDC2Parser.InnerJoinContext ctx);
	/**
	 * Exit a parse tree produced by {@link PDC2Parser#innerJoin}.
	 * @param ctx the parse tree
	 */
	void exitInnerJoin(PDC2Parser.InnerJoinContext ctx);
	/**
	 * Enter a parse tree produced by {@link PDC2Parser#leftJoin}.
	 * @param ctx the parse tree
	 */
	void enterLeftJoin(PDC2Parser.LeftJoinContext ctx);
	/**
	 * Exit a parse tree produced by {@link PDC2Parser#leftJoin}.
	 * @param ctx the parse tree
	 */
	void exitLeftJoin(PDC2Parser.LeftJoinContext ctx);
	/**
	 * Enter a parse tree produced by {@link PDC2Parser#groupBy}.
	 * @param ctx the parse tree
	 */
	void enterGroupBy(PDC2Parser.GroupByContext ctx);
	/**
	 * Exit a parse tree produced by {@link PDC2Parser#groupBy}.
	 * @param ctx the parse tree
	 */
	void exitGroupBy(PDC2Parser.GroupByContext ctx);
	/**
	 * Enter a parse tree produced by {@link PDC2Parser#groupByColumns}.
	 * @param ctx the parse tree
	 */
	void enterGroupByColumns(PDC2Parser.GroupByColumnsContext ctx);
	/**
	 * Exit a parse tree produced by {@link PDC2Parser#groupByColumns}.
	 * @param ctx the parse tree
	 */
	void exitGroupByColumns(PDC2Parser.GroupByColumnsContext ctx);
	/**
	 * Enter a parse tree produced by {@link PDC2Parser#dataSetName}.
	 * @param ctx the parse tree
	 */
	void enterDataSetName(PDC2Parser.DataSetNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link PDC2Parser#dataSetName}.
	 * @param ctx the parse tree
	 */
	void exitDataSetName(PDC2Parser.DataSetNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link PDC2Parser#columnName}.
	 * @param ctx the parse tree
	 */
	void enterColumnName(PDC2Parser.ColumnNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link PDC2Parser#columnName}.
	 * @param ctx the parse tree
	 */
	void exitColumnName(PDC2Parser.ColumnNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link PDC2Parser#execValue}.
	 * @param ctx the parse tree
	 */
	void enterExecValue(PDC2Parser.ExecValueContext ctx);
	/**
	 * Exit a parse tree produced by {@link PDC2Parser#execValue}.
	 * @param ctx the parse tree
	 */
	void exitExecValue(PDC2Parser.ExecValueContext ctx);
	/**
	 * Enter a parse tree produced by {@link PDC2Parser#valuesList}.
	 * @param ctx the parse tree
	 */
	void enterValuesList(PDC2Parser.ValuesListContext ctx);
	/**
	 * Exit a parse tree produced by {@link PDC2Parser#valuesList}.
	 * @param ctx the parse tree
	 */
	void exitValuesList(PDC2Parser.ValuesListContext ctx);
	/**
	 * Enter a parse tree produced by {@link PDC2Parser#value}.
	 * @param ctx the parse tree
	 */
	void enterValue(PDC2Parser.ValueContext ctx);
	/**
	 * Exit a parse tree produced by {@link PDC2Parser#value}.
	 * @param ctx the parse tree
	 */
	void exitValue(PDC2Parser.ValueContext ctx);
	/**
	 * Enter a parse tree produced by {@link PDC2Parser#nullValue}.
	 * @param ctx the parse tree
	 */
	void enterNullValue(PDC2Parser.NullValueContext ctx);
	/**
	 * Exit a parse tree produced by {@link PDC2Parser#nullValue}.
	 * @param ctx the parse tree
	 */
	void exitNullValue(PDC2Parser.NullValueContext ctx);
	/**
	 * Enter a parse tree produced by {@link PDC2Parser#timestamp}.
	 * @param ctx the parse tree
	 */
	void enterTimestamp(PDC2Parser.TimestampContext ctx);
	/**
	 * Exit a parse tree produced by {@link PDC2Parser#timestamp}.
	 * @param ctx the parse tree
	 */
	void exitTimestamp(PDC2Parser.TimestampContext ctx);
	/**
	 * Enter a parse tree produced by {@link PDC2Parser#number}.
	 * @param ctx the parse tree
	 */
	void enterNumber(PDC2Parser.NumberContext ctx);
	/**
	 * Exit a parse tree produced by {@link PDC2Parser#number}.
	 * @param ctx the parse tree
	 */
	void exitNumber(PDC2Parser.NumberContext ctx);
	/**
	 * Enter a parse tree produced by {@link PDC2Parser#bool}.
	 * @param ctx the parse tree
	 */
	void enterBool(PDC2Parser.BoolContext ctx);
	/**
	 * Exit a parse tree produced by {@link PDC2Parser#bool}.
	 * @param ctx the parse tree
	 */
	void exitBool(PDC2Parser.BoolContext ctx);
	/**
	 * Enter a parse tree produced by {@link PDC2Parser#string}.
	 * @param ctx the parse tree
	 */
	void enterString(PDC2Parser.StringContext ctx);
	/**
	 * Exit a parse tree produced by {@link PDC2Parser#string}.
	 * @param ctx the parse tree
	 */
	void exitString(PDC2Parser.StringContext ctx);
	/**
	 * Enter a parse tree produced by {@link PDC2Parser#stringValue}.
	 * @param ctx the parse tree
	 */
	void enterStringValue(PDC2Parser.StringValueContext ctx);
	/**
	 * Exit a parse tree produced by {@link PDC2Parser#stringValue}.
	 * @param ctx the parse tree
	 */
	void exitStringValue(PDC2Parser.StringValueContext ctx);
}