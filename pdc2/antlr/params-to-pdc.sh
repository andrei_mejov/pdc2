#!/bin/bash

PACKAGE="-package pdc2.core.antlr.params"

java -jar antlr-4.7.1-complete.jar $PACKAGE -o ./src-params/ Params.g4

cp ./src-params/*.java ../src/pdc2/core/antlr/params