#!/bin/bash

TEST_STRING="ном = выбрать из \"../pdc/misc/test.dat\"\
	первые 4;\
\
ном1 = выбрать из ном\
	столбцы\
		CODE\
		TITLE\
	первые 2\
	добавить\
		целое 1 как НовоеПоле1;\
\
показать ном1;"

echo $TEST_STRING

if [ "x$1" = "xproduction" ]; then
    PACKAGE="-package pdc2.core.antlr"
else
    PACKAGE=""
fi

rm -R ./src
mkdir ./src
cp ./PDC2.g4 ./src/
java -jar antlr-4.7.1-complete.jar $PACKAGE -o ./src/ PDC2.g4

if [ "x$1" != "xproduction" ]; then 
    pushd ./src
    javac -g -cp ../antlr-4.7.1-complete.jar *.java
    echo $TEST_STRING | java -cp .:../antlr-4.7.1-complete.jar org.antlr.v4.gui.TestRig PDC2 r -gui
    popd
fi
