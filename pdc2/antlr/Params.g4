grammar Params;

@lexer::members {
        boolean isStringValue = false;
}

r: param*;

param: ID EQ string;

string: stringValue;
stringValue: LITERAL;

TERM: [;];
EQ : '=';
LT : '<';
GT : '>';
LITERAL: ('\'' {isStringValue = true;} ~('\'')* {isStringValue = false;} '\'') | ('"' {isStringValue = true;} ~('"')* {isStringValue = false;} '"');
NUM: [-]?[0-9.]+;
ID: [A-Za-zА-Яа-я0-9.*_]+;
COMMENT: '/*' .*? '*/' {if (!isStringValue) skip();};
CODE: '{%' .*? '%}';
WS : [ \t\r\n]+ {if (!isStringValue) skip();};