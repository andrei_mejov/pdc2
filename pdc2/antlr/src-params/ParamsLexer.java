// Generated from Params.g4 by ANTLR 4.7.1
package pdc2.core.antlr.params;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class ParamsLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.7.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		TERM=1, EQ=2, LT=3, GT=4, LITERAL=5, NUM=6, ID=7, COMMENT=8, CODE=9, WS=10;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] ruleNames = {
		"TERM", "EQ", "LT", "GT", "LITERAL", "NUM", "ID", "COMMENT", "CODE", "WS"
	};

	private static final String[] _LITERAL_NAMES = {
		null, null, "'='", "'<'", "'>'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "TERM", "EQ", "LT", "GT", "LITERAL", "NUM", "ID", "COMMENT", "CODE", 
		"WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	        boolean isStringValue = false;


	public ParamsLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "Params.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	@Override
	public void action(RuleContext _localctx, int ruleIndex, int actionIndex) {
		switch (ruleIndex) {
		case 4:
			LITERAL_action((RuleContext)_localctx, actionIndex);
			break;
		case 7:
			COMMENT_action((RuleContext)_localctx, actionIndex);
			break;
		case 9:
			WS_action((RuleContext)_localctx, actionIndex);
			break;
		}
	}
	private void LITERAL_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 0:
			isStringValue = true;
			break;
		case 1:
			isStringValue = false;
			break;
		case 2:
			isStringValue = true;
			break;
		case 3:
			isStringValue = false;
			break;
		}
	}
	private void COMMENT_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 4:
			if (!isStringValue) skip();
			break;
		}
	}
	private void WS_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 5:
			if (!isStringValue) skip();
			break;
		}
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\fc\b\1\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\3\2\3\2\3\3\3\3\3\4\3\4\3\5\3\5\3\6\3\6\3\6\7\6#\n\6\f\6\16\6&\13"+
		"\6\3\6\3\6\3\6\3\6\3\6\7\6-\n\6\f\6\16\6\60\13\6\3\6\3\6\5\6\64\n\6\3"+
		"\7\5\7\67\n\7\3\7\6\7:\n\7\r\7\16\7;\3\b\6\b?\n\b\r\b\16\b@\3\t\3\t\3"+
		"\t\3\t\7\tG\n\t\f\t\16\tJ\13\t\3\t\3\t\3\t\3\t\3\t\3\n\3\n\3\n\3\n\7\n"+
		"U\n\n\f\n\16\nX\13\n\3\n\3\n\3\n\3\13\6\13^\n\13\r\13\16\13_\3\13\3\13"+
		"\4HV\2\f\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\3\2\t\3\2==\3\2"+
		"))\3\2$$\3\2//\4\2\60\60\62;\t\2,,\60\60\62;C\\aac|\u0412\u0451\5\2\13"+
		"\f\17\17\"\"\2k\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3"+
		"\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2"+
		"\3\27\3\2\2\2\5\31\3\2\2\2\7\33\3\2\2\2\t\35\3\2\2\2\13\63\3\2\2\2\r\66"+
		"\3\2\2\2\17>\3\2\2\2\21B\3\2\2\2\23P\3\2\2\2\25]\3\2\2\2\27\30\t\2\2\2"+
		"\30\4\3\2\2\2\31\32\7?\2\2\32\6\3\2\2\2\33\34\7>\2\2\34\b\3\2\2\2\35\36"+
		"\7@\2\2\36\n\3\2\2\2\37 \7)\2\2 $\b\6\2\2!#\n\3\2\2\"!\3\2\2\2#&\3\2\2"+
		"\2$\"\3\2\2\2$%\3\2\2\2%\'\3\2\2\2&$\3\2\2\2\'(\b\6\3\2(\64\7)\2\2)*\7"+
		"$\2\2*.\b\6\4\2+-\n\4\2\2,+\3\2\2\2-\60\3\2\2\2.,\3\2\2\2./\3\2\2\2/\61"+
		"\3\2\2\2\60.\3\2\2\2\61\62\b\6\5\2\62\64\7$\2\2\63\37\3\2\2\2\63)\3\2"+
		"\2\2\64\f\3\2\2\2\65\67\t\5\2\2\66\65\3\2\2\2\66\67\3\2\2\2\679\3\2\2"+
		"\28:\t\6\2\298\3\2\2\2:;\3\2\2\2;9\3\2\2\2;<\3\2\2\2<\16\3\2\2\2=?\t\7"+
		"\2\2>=\3\2\2\2?@\3\2\2\2@>\3\2\2\2@A\3\2\2\2A\20\3\2\2\2BC\7\61\2\2CD"+
		"\7,\2\2DH\3\2\2\2EG\13\2\2\2FE\3\2\2\2GJ\3\2\2\2HI\3\2\2\2HF\3\2\2\2I"+
		"K\3\2\2\2JH\3\2\2\2KL\7,\2\2LM\7\61\2\2MN\3\2\2\2NO\b\t\6\2O\22\3\2\2"+
		"\2PQ\7}\2\2QR\7\'\2\2RV\3\2\2\2SU\13\2\2\2TS\3\2\2\2UX\3\2\2\2VW\3\2\2"+
		"\2VT\3\2\2\2WY\3\2\2\2XV\3\2\2\2YZ\7\'\2\2Z[\7\177\2\2[\24\3\2\2\2\\^"+
		"\t\b\2\2]\\\3\2\2\2^_\3\2\2\2_]\3\2\2\2_`\3\2\2\2`a\3\2\2\2ab\b\13\7\2"+
		"b\26\3\2\2\2\f\2$.\63\66;@HV_\b\3\6\2\3\6\3\3\6\4\3\6\5\3\t\6\3\13\7";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}