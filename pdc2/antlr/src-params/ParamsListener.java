// Generated from Params.g4 by ANTLR 4.7.1
package pdc2.core.antlr.params;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link ParamsParser}.
 */
public interface ParamsListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link ParamsParser#r}.
	 * @param ctx the parse tree
	 */
	void enterR(ParamsParser.RContext ctx);
	/**
	 * Exit a parse tree produced by {@link ParamsParser#r}.
	 * @param ctx the parse tree
	 */
	void exitR(ParamsParser.RContext ctx);
	/**
	 * Enter a parse tree produced by {@link ParamsParser#param}.
	 * @param ctx the parse tree
	 */
	void enterParam(ParamsParser.ParamContext ctx);
	/**
	 * Exit a parse tree produced by {@link ParamsParser#param}.
	 * @param ctx the parse tree
	 */
	void exitParam(ParamsParser.ParamContext ctx);
	/**
	 * Enter a parse tree produced by {@link ParamsParser#string}.
	 * @param ctx the parse tree
	 */
	void enterString(ParamsParser.StringContext ctx);
	/**
	 * Exit a parse tree produced by {@link ParamsParser#string}.
	 * @param ctx the parse tree
	 */
	void exitString(ParamsParser.StringContext ctx);
	/**
	 * Enter a parse tree produced by {@link ParamsParser#stringValue}.
	 * @param ctx the parse tree
	 */
	void enterStringValue(ParamsParser.StringValueContext ctx);
	/**
	 * Exit a parse tree produced by {@link ParamsParser#stringValue}.
	 * @param ctx the parse tree
	 */
	void exitStringValue(ParamsParser.StringValueContext ctx);
}